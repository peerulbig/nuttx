/*
 * slotlists_containter_s.h
 *
 *  Created on: 05.03.2018
 *      Author: bbrandt
 */

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_SLOTLISTS_CONTAINER_S_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_SLOTLISTS_CONTAINER_S_H_

#include "slotlist_s.h"


#if defined(__cplusplus)
namespace Physikschicht
{
#endif

/* This struct contains pointers to our slotlists. For the client we only
 * support the use of one channel anyway. But to achieve highest similarity
 * to the master we stuck to this nomenclature.
 */
struct slotlists_container_s
{
#ifdef CONFIG_MESSBUS_MASTER
# if CONFIG_MESSBUS_USE_CH1
	slotlist1_s *slotlist1;
# elif CONFIG_MESSBUS_USE_CH2
	slotlist2_s *slotlist2;
# elif CONFIG_MESSBUS_USE_CH3
	slotlist3_s *slotlist3;
# elif CONFIG_MESSBUS_USE_CH4
	slotlist4_s *slotlist4;
# endif
#elif CONFIG_MESSBUS_CLIENT
	slotlist1_s *slotlist1;
#endif
};

#if defined(__cplusplus)
}
#endif

#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_SLOTLISTS_CONTAINER_S_H_ */
