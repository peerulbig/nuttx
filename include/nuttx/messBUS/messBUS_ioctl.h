/*
 * messBUS_ioctl.h
 *
 *  Created on: 20.03.2018
 *      Author: bbrandt
 */

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_IOCTL_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_IOCTL_H_

/* ioctl commands */

/* Command:     MESSBUSIOC_START
 * Description: Start the messBUS. This assumes that the ioctl command
 * 				MESSBUSIOC_ATTACH_SLOTLISTS has already been called
 * 				successfully, otherwise the messBUS does not know what to do.
 * Argument:    NULL
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_START  _MESSBUSIOC(0x0001)

/* Command:     MESSBUSIOC_ATTACH_SLOTLISTS
 * Description: Attach a slotlist container to the messBUS. For the messBUS
 * 				client, the slotlist container will consist only of one
 * 				pointer to a single slotlist, as we only support one channel
 * 				for the client.
 * Argument:    A pointer to an instance of struct slotlists_container_s.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_ATTACH_SLOTLISTS     _MESSBUSIOC(0x0002)

/* Command:     MESSBUSIOC_STOP
 * Description: Stop the messBUS.
 * Argument:    NULL
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_STOP     _MESSBUSIOC(0x0003)

/* Command:     MESSBUSIOC_SYNCWAIT
 * Description: This call blocks until a SYNC occurs.
 * Argument:    NULL or a pointer to a uint8_t where to write the number of
 * 				already missed SYNCS.
 * Return:      Zero (OK) on success.  Minus one will be returned in case of
 * 				already missed SYNCS.
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_SYNCWAIT     _MESSBUSIOC(0x0004)

/* Command:     MESSBUSIOC_UPDATE_RXBUFFERS
 * Description: Invalidate cache before reading from rxbuffers. This call is
 * 				only needed for STM32F7 series MCUs.
 * Argument:    NULL
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_UPDATE_RXBUFFERS     _MESSBUSIOC(0x0005)

/* Command:     MESSBUSIOC_GET_PHASE_DEVIATION
 * Description: Get the phase deviation in multiples of 250ns. 250ns
 * 				represent one timer tick. Positive values represent delays.
 * Argument:    Pointer to int8_t where to write the phase deviation in
 * 				timer ticks (250ns).
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_GET_PHASE_DEVIATION     _MESSBUSIOC(0x0006)

/* Command:     MESSBUSIOC_SET_PHASE_CORRECTION
 * Description: Set the phase correction in multiples of 250ns. 250ns
 * 				represent one timer tick. This setting will be valid until
 * 				another MESSBUSIOC_SET_PHASE_CORRECTION call overwrites.
 * 				Positive values will cause the driver to add extra delays.
 * 				The setting will only have effect after another call to
 * 				MESSBUSIOC_ATTACH_SLOTLISTS.
 * Argument:    Pointer to int8_t where to write the phase correction in
 * 				timer ticks (250ns).
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_SET_PHASE_CORRECTION     _MESSBUSIOC(0x0007)




#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_IOCTL_H_ */
