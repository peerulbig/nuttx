/****************************************************************************
 * include/nuttx/messBUS/messBUSClient.h
 *
 *   Author: Peer Ulbig <p.ulbig@tu-braunschweig.de>
 *
 ****************************************************************************/

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUSCLIENT_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUSCLIENT_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/fs/ioctl.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#include "messBUS_config.h"
#include "messBUS_ioctl.h"

/****************************************************************************
 * Public Types
 ****************************************************************************/
#include "slot_s.h"
#include "slotlist_s.h"
#include "slotlists_container_s.h"

/* This structure defines all of the operations provided by the architecture
 * specific logic.  All fields must be provided with non-NULL function pointers
 * by the caller of messBUS_register().
 */
struct messBUS_ops_s
{
  /* Setup the messBUS. */
  CODE int (*setup)(void);

  /* Disable the messBUS. */
  CODE int (*shutdown)(void);

  /* Start the messBUS. */
  CODE int (*start)(void);

  /* Stop the messBUS. */
  CODE int (*stop)(void);

  /* Convert the slotlist. */
  CODE void (*convert_slotlist)(void *slotlist, void *actiontable, void *infotable);

  /* Update buffers */
  CODE void (*update_buffers)(void);
};

/* This is the device structure used by the driver.  The caller of
 * messBUSClient_register() must allocate and initialize this structure.
 */
struct messBUS_dev_s
{
  /* State data */
	uint8_t		open;
	uint8_t		running;
	uint8_t		sync_count;
	uint8_t		active_table;
	uint8_t		new_table_avail;
	//int8_t	phase_dev;
	uint16_t	phase_dev;
	int8_t		phase_cor;
	uint16_t	period_ticks;

  /* Pointers to tables for the interrupt routine */
	void		*isr_actiontable0;
	void		*isr_actiontable1;
	void		*isr_infotable0;
	void		*isr_infotable1;

  /* Arch-specific operations as driver interface */
  FAR const struct messBUS_ops_s 	*ops;
};
typedef struct messBUS_dev_s messBUS_dev_t;

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

/************************************************************************************
 * Name: messBUSClient_register
 *
 * Description:
 *   Register the messBUSClient in the VFS.
 *
 ************************************************************************************/

int messBUSClient_register(FAR const char *path, FAR messBUS_dev_t *dev);

/****************************************************************************
 * Name: messBUSClient_initialize
 *
 * Description:
 *   Perform initialization and registration of the driver.
 *	 This method should be called from board-specific stm32_bringup.c
 *
 ****************************************************************************/

void messBUSClient_initialize(void);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUSMASTER_H_ */
