/*
 * slotlist_s.h
 *
 *  Created on: 05.03.2018
 *      Author: bbrandt
 */

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_SLOTLIST_S_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_SLOTLIST_S_H_

#include <nuttx/config.h>
#include "messBUS_config.h"
#include "slot_s.h"

#if defined(__cplusplus)
namespace Physikschicht
{
#endif

#ifdef CONFIG_MESSBUS_MASTER
# if CONFIG_MESSBUS_USE_CH1
#   define NUMSLOTS (CONFIG_MESSBUS_CH1_MAX_RX_SLOTS + CONFIG_MESSBUS_CH1_MAX_TX_SLOTS)
# elif CONFIG_MESSBUS_USE_CH2
#  define NUMSLOTS (CONFIG_MESSBUS_CH2_MAX_RX_SLOTS + CONFIG_MESSBUS_CH2_MAX_TX_SLOTS)
# elif CONFIG_MESSBUS_USE_CH3
#  define NUMSLOTS (CONFIG_MESSBUS_CH3_MAX_RX_SLOTS + CONFIG_MESSBUS_CH3_MAX_TX_SLOTS)
# elif CONFIG_MESSBUS_USE_CH4
#  define NUMSLOTS (CONFIG_MESSBUS_CH4_MAX_RX_SLOTS + CONFIG_MESSBUS_CH4_MAX_TX_SLOTS)
# endif

#elif CONFIG_MESSBUS_CLIENT
#  define NUMSLOTS (CONFIG_MESSBUS_CH1_MAX_RX_SLOTS + CONFIG_MESSBUS_CH1_MAX_TX_SLOTS)
#else
#  define NUMSLOTS 20
#endif

typedef struct slotlist_s
{
	uint8_t n_slots;
	struct slot_s slot[NUMSLOTS];
} slotlist_s;

typedef slotlist_s slotlist1_s;
typedef slotlist_s slotlist2_s;
typedef slotlist_s slotlist3_s;
typedef slotlist_s slotlist4_s;

#if defined(__cplusplus)
}
#endif

#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_SLOTLIST_S_H_ */
