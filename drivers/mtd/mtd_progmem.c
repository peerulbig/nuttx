/****************************************************************************
 * drivers/mtd/mtd_progmem.c
 *
 *   Copyright (C) 2015 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include <nuttx/progmem.h>
#include <nuttx/fs/ioctl.h>
#include <nuttx/mtd/mtd.h>

#if defined(CONFIG_STM32F7_STM32F74XX) || defined(CONFIG_STM32_STM32F4XXX)

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
/* Because STM32 flash is not uniform we only use two single pages 
 * (= erase block = sector) of physical flash. 
 * Here we select which ones we use.
 */
#if defined(CONFIG_STM32F7_STM32F74XX) || defined(CONFIG_STM32F7_STM32F75XX)
# define STM32_MTD_PAGENO   6 //  2 last sectors (256 + 256 KB)
# define STM32_MTD_PAGENUM 2
#elif defined (CONFIG_STM32_STM32F4XXX)
# define STM32_MTD_PAGENO  10       // 2 last sectors (128 + 128 KB)
# define STM32_MTD_PAGENUM 2
#endif

/****************************************************************************
 * Private Types
 ****************************************************************************/

/* This type represents the state of the MTD device.  The struct mtd_dev_s
 * must appear at the beginning of the definition so that you can freely
 * cast between pointers to struct mtd_dev_s and struct progmem_dev_s.
 */

struct progmem_dev_s
{
  /* Publically visible representation of the interface */

  struct mtd_dev_s mtd;

  /* Fields unique to the progmem MTD driver */

  bool    initialized;      /* True: Already initialized */
  uint8_t pageno;           /* This is the first page we use 
                             * (virtual flash base)
                             * */
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Internal helper functions */

/* MTD driver methods */

static int     progmem_erase(FAR struct mtd_dev_s *dev, off_t startblock,
                 size_t nblocks);
static ssize_t progmem_bread(FAR struct mtd_dev_s *dev, off_t startblock,
                 size_t nblocks, FAR uint8_t *buf);
static ssize_t progmem_bwrite(FAR struct mtd_dev_s *dev, off_t startblock,
                 size_t nblocks, FAR const uint8_t *buf);
static ssize_t progmem_read(FAR struct mtd_dev_s *dev, off_t offset,
                 size_t nbytes, FAR uint8_t *buffer);
#ifdef CONFIG_MTD_BYTE_WRITE
static ssize_t progmem_write(FAR struct mtd_dev_s *dev, off_t offset,
                 size_t nbytes, FAR const uint8_t *buffer);
#endif
static int     progmem_ioctl(FAR struct mtd_dev_s *dev, int cmd,
                 unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/
/* This structure holds the state of the MTD driver */

static struct progmem_dev_s g_progmem =
{
  {
    progmem_erase,
    progmem_bread,
    progmem_bwrite,
    progmem_read,
#ifdef CONFIG_MTD_BYTE_WRITE
    progmem_write,
#endif
    progmem_ioctl
  }
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: isParamOk
 *
 * Description: Check if parameters for erase operation are valid.
 *
 ****************************************************************************/

static int isParamOk(off_t startblock, size_t nblocks)
{
	if (!(startblock >= STM32_MTD_PAGENO && startblock < STM32_MTD_PAGENO + STM32_MTD_PAGENUM)
			|| nblocks > STM32_MTD_PAGENUM)
		return !OK;

	return OK;
}

/****************************************************************************
 * Name: progmem_erase
 *
 * Description:
 *   Erase several blocks, each of the size previously reported.
 *
 ****************************************************************************/

static int progmem_erase(FAR struct mtd_dev_s *dev, off_t startblock,
                         size_t nblocks)
{
  ssize_t result;

	if(isParamOk(startblock, nblocks) != OK) return -1;


  /* Erase the specified blocks and return status (OK or a negated errno) */

  while (nblocks > 0)
    {
      result = up_progmem_erasepage(startblock);
      if (result < 0)
      {
          return (int)result;
      }

      /* Setup for the next pass through the loop */

      startblock++;
      nblocks--;
    }

  return OK;
}

/****************************************************************************
 * Name: progmem_bread
 *
 * Description:
 *   Read the specified number of blocks into the user provided buffer.
 *
 ****************************************************************************/

static ssize_t progmem_bread(FAR struct mtd_dev_s *dev, off_t startblock,
                             size_t nblocks, FAR uint8_t *buffer)
{
	FAR struct progmem_dev_s *priv = (FAR struct progmem_dev_s *)dev;
    FAR const uint8_t *src;


  /* Read the specified blocks into the provided user buffer and return
   * status (The positive, number of blocks actually read or a negated
   * errno).
   */

    /* check parameters and calc offset values */
	size_t offset_bytes = startblock*up_progmem_blocksize();
	size_t page_size = up_progmem_pagesize(priv->pageno);
	uint8_t page_offset = offset_bytes/page_size;

	if(page_offset > 0)
	{
		offset_bytes -= page_offset*page_size;
	}

	if(page_offset > STM32_MTD_PAGENUM - 1)
        return -EINVAL;

	src = (FAR const uint8_t *)up_progmem_getaddress(priv->pageno + page_offset) + offset_bytes;
	size_t count = nblocks*up_progmem_blocksize();

	size_t ret_bytes = up_progmem_read(src, buffer, count);
	size_t ret_blocks = ret_bytes / up_progmem_blocksize();

	return ret_blocks;
}

/****************************************************************************
 * Name: progmem_bwrite
 *
 * Description:
 *   Write the specified number of blocks from the user provided buffer.
 *
 ****************************************************************************/

static ssize_t progmem_bwrite(FAR struct mtd_dev_s *dev, off_t startblock,
                              size_t nblocks, FAR const uint8_t *buffer)
{
	FAR struct progmem_dev_s *priv = (FAR struct progmem_dev_s *)dev;
   ssize_t result;

   /* Write the specified blocks from the provided user buffer and return status
   * (The positive, number of blocks actually written or a negated errno)
   */

	size_t offset_bytes = startblock*up_progmem_blocksize();
	size_t page_size = up_progmem_pagesize(priv->pageno);
	uint8_t page_offset = offset_bytes/page_size;

	if(page_offset > 0)
	{
		offset_bytes -= page_offset*page_size;
	}

	if(page_offset > STM32_MTD_PAGENUM - 1)
        return -EINVAL;

	size_t address =up_progmem_getaddress(priv->pageno + page_offset) + offset_bytes;
	size_t count = nblocks*up_progmem_blocksize();

	size_t ret_bytes = up_progmem_write(address, buffer, count);
	size_t ret_blocks = ret_bytes / up_progmem_blocksize();

    return ret_blocks;
}

/****************************************************************************
 * Name: progmem_read
 *
 * Description:
 *   Read the specified number of bytes to the user provided buffer.
 *
 *   offset: number of blocks relative to beginning of used flash part (e.g. if
 *   we use only page 6 and 7, this is relative to the beginning of part 6.)
 *
 ****************************************************************************/

static ssize_t progmem_read(FAR struct mtd_dev_s *dev, off_t offset,
                            size_t nbytes, FAR uint8_t *buffer)
{
	FAR struct progmem_dev_s *priv = (FAR struct progmem_dev_s *)dev;
  FAR const uint8_t *src;

  /* Read the specified bytes into the provided user buffer and return
   * status (The positive, number of bytes actually read or a negated
   * errno)
   */

	/* make sure that we are going to use right page for adress finding */
	size_t offset_bytes = offset;
	size_t page_size = up_progmem_pagesize(priv->pageno);
	uint8_t page_offset = offset_bytes/page_size;

	if(page_offset > 0)
	{
		offset_bytes -= page_offset*page_size;
	}

	if(page_offset > STM32_MTD_PAGENUM - 1)
        return -EINVAL;

	src = (FAR const uint8_t *)up_progmem_getaddress(priv->pageno + page_offset) + offset_bytes;

	if(up_progmem_getpage(src) != priv->pageno + page_offset)
        return -EINVAL;

    return up_progmem_read(src,buffer,nbytes);
}

/****************************************************************************
 * Name: progmem_write
 *
 * Description:
 *   Some FLASH parts have the ability to write an arbitrary number of
 *   bytes to an arbitrary offset on the device.
 *
 ****************************************************************************/

#ifdef CONFIG_MTD_BYTE_WRITE
static ssize_t progmem_write(FAR struct mtd_dev_s *dev, off_t offset,
                             size_t nbytes, FAR const uint8_t *buffer)
{
  FAR struct progmem_dev_s *priv = (FAR struct progmem_dev_s *)dev;
  FAR const uint8_t *addr;
  off_t startblock;
  ssize_t result;

  /* Write the specified blocks from the provided user buffer and return status
   * (The positive, number of blocks actually written or a negated errno)
   */

  size_t offset_bytes = offset;
  size_t page_size = up_progmem_pagesize(priv->pageno);
  uint8_t page_offset = offset_bytes/page_size;

  if(page_offset > 0)
  {
      offset_bytes -= page_offset*page_size;
  }

  if(page_offset > STM32_MTD_PAGENUM - 1)
      return -EINVAL;

  addr = up_progmem_getaddress(priv->pageno + page_offset) + offset_bytes;

  result = up_progmem_write(addr, buffer, nbytes);

  return result < 0 ? result : nbytes;
}
#endif

/****************************************************************************
 * Name: progmem_ioctl
 ****************************************************************************/

static int progmem_ioctl(FAR struct mtd_dev_s *dev, int cmd, unsigned long arg)
{
	FAR struct progmem_dev_s *priv = (FAR struct progmem_dev_s *)dev;
  int ret = -EINVAL; /* Assume good command with bad parameters */

  switch (cmd)
    {
      case MTDIOC_GEOMETRY:
        {
          FAR struct mtd_geometry_s *geo = (FAR struct mtd_geometry_s *)arg;
          if (geo)
            {
              /* Populate the geometry structure with information needed to know
               * the capacity and how to access the device.
               *
               * NOTE: We ensure in initialize function that we only use one 
				 * erase block (= physical sector) of flash device.
               */

				geo->blocksize = up_progmem_blocksize();            /* Size of one read/write block */
				geo->erasesize = up_progmem_pagesize(priv->pageno); /* Size of one erase block */
				geo->neraseblocks = STM32_MTD_PAGENUM;              /* Number of erase blocks */
				ret = OK;
          }
        }
        break;

      case MTDIOC_XIPBASE:
        {
          FAR void **ppv = (FAR void**)arg;

          if (ppv)
            {
              /* Return (void*) our virtual base address of FLASH memory. */

				*ppv = (FAR void *)up_progmem_getaddress(priv->pageno);
              ret  = OK;
            }
        }
        break;

      case MTDIOC_BULKERASE:
        {
          /* Erase the entire device */

			ret = progmem_erase(dev, priv->pageno, STM32_MTD_PAGENUM);
        }
        break;

      default:
        ret = -ENOTTY; /* Bad command */
        break;
    }

  return ret;
}



/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: progmem_initialize
 *
 * Description:
 *   Create and initialize an MTD device instance that can be used to access
 *   on-chip program memory.
 *
 *   MTD devices are not registered in the file system, but are created as
 *   instances that can be bound to other functions (such as a block or
 *   character driver front end).
 *
 ****************************************************************************/

FAR struct mtd_dev_s *progmem_initialize(void)
{
  FAR struct progmem_dev_s *priv = (FAR struct progmem_dev_s *)&g_progmem;

  /* Perform initialization if necessary */

  if (!g_progmem.initialized)
    {

		g_progmem.pageno = STM32_MTD_PAGENO;
      g_progmem.initialized = true;

#ifdef CONFIG_MTD_REGISTRATION
      /* Register the MTD with the procfs system if enabled */

      mtd_register(&priv->mtd, "progmem");
#endif
    }

  /* Return the implementation-specific state structure as the MTD device */

  return (FAR struct mtd_dev_s *)priv;
}

#endif /* #if defined(CONFIG_STM32F7_STM32F74XX) || defined(CONFIG_STM32_STM32F40XX) */
