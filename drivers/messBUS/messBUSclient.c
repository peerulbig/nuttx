/****************************************************************************
 * nuttx/drivers/messBUS/messBUSclient.c
 *
 *   Author: Peer Ulbig <p.ulbig@tu-braunschweig.de>
 *
 ****************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <poll.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/sched.h>
#include <nuttx/fs/fs.h>
#include <nuttx/messBUS/messBUSClient.h>
#include <nuttx/fs/ioctl.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/************************************************************************************
 * Private Types
 ************************************************************************************/

/************************************************************************************
 * Private Function Prototypes
 ************************************************************************************/

/* Character driver methods */
typedef FAR struct file		file_t;
static int     messBUSClient_open(file_t *filep);
static int     messBUSClient_close(file_t *filep);
static ssize_t messBUSClient_read(file_t *filep, FAR char *buffer, size_t buflen);
static ssize_t messBUSClient_write(file_t *filep, FAR const char *buf, size_t buflen);
static int     messBUSClient_ioctl(FAR struct file *filep, int cmd, unsigned long arg);

/************************************************************************************
 * Private Data
 ************************************************************************************/

static const struct file_operations g_messBUSClient_ops = {
	messBUSClient_open,		/* open */
	messBUSClient_close,	/* close */
	messBUSClient_read,		/* read */
	messBUSClient_write,	/* write */
	0,						/* seek */
	messBUSClient_ioctl		/* ioctl */
#ifndef CONFIG_DISABLE_POLL
	, 0 					/* poll */
#endif
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
	, NULL      			/* unlink */
#endif
};

/************************************************************************************
 * Private Functions
 ************************************************************************************/

static int messBUSClient_open(file_t *filep)
{
	FAR struct inode *inode = filep->f_inode;
	FAR messBUS_dev_t *dev = inode->i_private;
	int ret;
	int error = 0;	// OK

	if(dev->open)
	{
		/* Device has already been opened, so deny the access.
		 * We don't wont to support multiple opens yet to keep it
		 * simple. */
		error = -EACCES;
	}
	else
	{
		/* Open the device and perform initial setup. */
		dev->open = 1;
		ret = dev->ops->setup();
		if (ret < 0)
			{
				// Error handling ???
			}
	}

	return error; // error probably = 0 = OK
}

static int messBUSClient_close(file_t *filep)
{
	/* Nothing to do here */

	return 0;
}

static ssize_t messBUSClient_read(file_t *filep, FAR char *buf, size_t buflen)
{
	/* Nothing to do here */

	return 0;
}

static ssize_t messBUSClient_write(file_t *filep, FAR const char *buf, size_t buflen)
{
	/* Nothing to do here */

	return 0;
}

static int messBUSClient_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
	FAR struct inode *inode = filep->f_inode;
	messBUS_dev_t *dev = inode->i_private;
	int ret;

	switch(cmd)
	{
	case MESSBUSIOC_START:

		/* Make sure that there are already tables attached to the device.
		 * Reset the flag so that the interrupt handler does not switch
		 * to the empty tables immediately.
		 */
		if (dev->new_table_avail)
		{
			dev->new_table_avail = 0;
			ret = dev->ops->start();
			if (ret < 0)
				{
					// Error handling ???
				}
			dev->running = 1;
		}
		break;

	case MESSBUSIOC_ATTACH_SLOTLISTS:

		/* Cast the unsigned long arg to the address of our slotlist container.
		 * This container only contains one slotlist (slotlist1) for the client.
		 * The container is used to provide compatibility of the ioctl interfaces
		 * with respect to the master, which can operate up to four slotlists.
		 * The master's four slotlists have to be packed into one slotlist
		 * container as we can only parse one pointer to the ioctl function.
		 */
		; // Perform a dummy statement to satisfy the compiler
		struct slotlists_container_s *slotlists_container_ptr = (struct slotlists_container_s *) ((uintptr_t)arg);
		slotlist1_s *slotlist1_ptr = slotlists_container_ptr->slotlist1;

		/* If the driver is already running convert the slotlist to the currently
		 * inactive isr tables. If the driver is not running, always use tables
		 * with index 0 which the driver will start with by default.
		 */
		if (dev->running)
		{
			if (dev->active_table)
			{
				dev->ops->convert_slotlist(slotlist1_ptr, dev->isr_actiontable0, dev->isr_infotable0);
			}
			else
			{
				dev->ops->convert_slotlist(slotlist1_ptr, dev->isr_actiontable1, dev->isr_infotable1);
			}
		}
		else
		{
			dev->ops->convert_slotlist(slotlist1_ptr, dev->isr_actiontable0, dev->isr_infotable0);
		}

		/* Signal that there are new tables available */
		dev->new_table_avail = 1;

		break;

	case MESSBUSIOC_SYNCWAIT:

		/* Wait for the sync_counter to become non-zero.
		 *
		 * Hmmm... Maybe arg should be a pointer to uint8 in order to
		 * provide feedback about missed syncs in case sync_counter > 1.
		 *
		 * Be aware of the fact that usleep waits at least one microsecond.
		 * Depending on CONFIG_USEC_PER_TICK the sleep can take much longer
		 * than the specified microsecond. Furthermore this is not a busy
		 * wait but a suspension of the calling thread until the scheduler
		 * reawakens it.
		 */
		while(dev->sync_count < 1)
		{
			//usleep(CONFIG_MESSBUS_SYNCWAIT_SLEEP);
		}

		/* Reset the sync_counter before returning. */
		dev->sync_count = 0;

		break;

	case MESSBUSIOC_STOP:

		/* Not implemented yet */
		break;


	case MESSBUSIOC_UPDATE_RXBUFFERS:
		/* make the lower half update the buffers */
		dev->ops->update_buffers();

		break;

	case MESSBUSIOC_GET_PHASE_DEVIATION:

		; // Perform a dummy statement to satisfy the compiler
//		int8_t *phase_ptr = (int8_t *) ((uintptr_t)arg);
//		*phase_ptr = dev->phase_dev;
		uint16_t *phase_ptr = (uint16_t *) ((uintptr_t)arg);
		//*phase_ptr = dev->period_ticks;
		*phase_ptr = dev->phase_dev;
		break;


	case MESSBUSIOC_SET_PHASE_CORRECTION:

		; // Perform a dummy statement to satisfy the compiler
		int8_t *cor_ptr = (int8_t *) ((uintptr_t)arg);
		if (*cor_ptr != 0)
		{
			dev->phase_cor = *cor_ptr;
		}

		break;

	}

	return 0;
}


/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: messBUSClient_register
 *
 * Description:
 *   Register the upper half with its lower half in the VFS.
 *
 ************************************************************************************/

int messBUSClient_register(FAR const char *path, FAR messBUS_dev_t *dev)
{
  /* Register the driver */
  return register_driver(path, &g_messBUSClient_ops, 0444, dev);
}
