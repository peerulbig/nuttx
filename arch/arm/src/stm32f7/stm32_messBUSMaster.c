/****************************************************************************
 * arch/arm/src/stm32f7/stm32_messBUSMaster.c
 *
 *   Author: Peer Ulbig <p.ulbig@tu-braunschweig.de>
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/arch.h>
#include <nuttx/irq.h>
#include <arch/board/board.h>
#include <arch/chip/chip.h>
#include <arch/irq.h>
#include <arch/chip/irq.h>
#include <debug.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <sched.h>
#include <stdio.h>						// printf

#include "up_arch.h" 					// putreg32, modifyreg16 etc.
#include "chip.h"
#include "stm32_gpio.h"
#include "chip/stm32_tim.h"
#include "chip/stm32_gpio.h"
#include "chip/stm32_uart.h"
#include "up_internal.h"
#include "ram_vectors.h"
#include "stm32_dma.h"
#include "stm32_rcc.h"
#include "stm32_uart.h"
#include "stm32_tim.h"
#ifdef CONFIG_ARCH_CHIP_STM32F7
	#include "cache.h"
#endif
#include "stm32_messBUSMaster.h"

/* Header file which is also used in application and board specific bringup.c */
#include <nuttx/messBUS/messBUSMaster.h>

/****************************************************************************
 * Non-atomic hardware access helper functions
 ****************************************************************************/

/****************************************************************************
 * Name: setbit32
 *
 * Description:
 *   Set a single bit in 32-bit register *target with read-modify-write operation.
 *   Suppress compiler optimizations using volatile and avoid extra clock
 *   cycles for function call by declaring the function always inline.
 *
 ****************************************************************************/

static inline void setbit32(unsigned int target, uint32_t bit) __attribute__((always_inline));
static inline void setbit32(unsigned int target, uint32_t bit){
	*(volatile uint32_t *)(target) |= (bit);
};

/****************************************************************************
 * Name: clearbit32
 *
 * Description:
 *   Clear a single bit in 32-bit register *target with read-modify-write operation.
 *   Suppress compiler optimizations using volatile and avoid extra clock
 *   cycles for function call by declaring the function always inline.
 *
 ****************************************************************************/

static inline void clearbit32(unsigned int target, uint32_t bit) __attribute__((always_inline));
static inline void clearbit32(unsigned int target, uint32_t bit){
	*(volatile uint32_t *)(target) &= ~(bit);
};

/****************************************************************************
 * Name: setbit16
 *
 * Description:
 *   Set a single bit in 16-bit register *target with read-modify-write operation.
 *   Suppress compiler optimizations using volatile and avoid extra clock
 *   cycles for function call by declaring the function always inline.
 *
 ****************************************************************************/

static inline void setbit16(unsigned int target, uint16_t bit) __attribute__((always_inline));
static inline void setbit16(unsigned int target, uint16_t bit){
	*(volatile uint16_t *)(target) |= (bit);
};

/****************************************************************************
 * Name: clearbit16
 *
 * Description:
 *   Clear a single bit in 16-bit register *target with read-modify-write operation.
 *   Suppress compiler optimizations using volatile and avoid extra clock
 *   cycles for function call by declaring the function always inline.
 *
 ****************************************************************************/

static inline void clearbit16(unsigned int target, uint16_t bit) __attribute__((always_inline));
static inline void clearbit16(unsigned int target, uint16_t bit){
	*(volatile uint16_t *)(target) &= ~(bit);
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static uint16_t up_baud_to_brr (uint32_t baud, uint32_t fck);
static void up_convert_slotlist(void *container, void *actiontable, void *infocontainer);
static void tim3_handler(void);
static int up_setup(void);
static int up_shutdown(void);
static int up_start(void);
static int up_stop(void);
static void up_update_buffers(void);

/****************************************************************************
 * Private Types
 ****************************************************************************/

struct up_isr_action_s
{
	uint16_t 	ccr3;			// Next counter value to program into Timer 3 CCR3 register
	uint8_t		op_mode;		// Next ISR operation mode / case
};

struct up_isr_info_s
{
	uint16_t	brr;
	char		*buffer;
	uint16_t	buf_length;
	uint16_t	*buf_length_ptr;
};

struct up_isr_info_container_s
{
#if CONFIG_MESSBUS_USE_CH1
	struct up_isr_info_s	*info1;
#endif
#if CONFIG_MESSBUS_USE_CH2
	struct up_isr_info_s	*info2;
#endif
#if CONFIG_MESSBUS_USE_CH3
	struct up_isr_info_s	*info3;
#endif
#if CONFIG_MESSBUS_USE_CH4
	struct up_isr_info_s	*info4;
#endif
};

/****************************************************************************
 * Private Data
 ****************************************************************************/

/* Arch-specific instance of messBUS_ops_s, which will be attached to
 * g_messBUSpriv and thus to the upper half driver.
 */
static const struct messBUS_ops_s g_messBUS_ops =
{
  .setup			= up_setup,
  .shutdown			= up_shutdown,
  .start			= up_start,
  .stop				= up_stop,
  .convert_slotlist	= up_convert_slotlist,
  .update_buffers	= up_update_buffers
};

/* Default operation mode for next action */
static volatile uint8_t g_op_mode = 0;

/* Action tables and respective pointer. We always need a further action for
 * SYNC preparation (case 2) at the end of each timeslice.
 */
static struct up_isr_action_s isr_actiontable0[mB_NACTIONS + 1];
static struct up_isr_action_s isr_actiontable1[mB_NACTIONS + 1];
static volatile struct up_isr_action_s *isr_actiontable_ptr = isr_actiontable0;

/* Isr info tables */
#if CONFIG_MESSBUS_USE_CH1
	static struct up_isr_info_s isr_ch1infotable0[mB_CH1_NSLOTS] = {0};
	static struct up_isr_info_s isr_ch1infotable1[mB_CH1_NSLOTS] = {0};
	static volatile struct up_isr_info_s *isr_ch1infotable_ptr = isr_ch1infotable0;
#endif
#if CONFIG_MESSBUS_USE_CH2
	static struct up_isr_info_s isr_ch2infotable0[mB_CH2_NSLOTS] = {0};
	static struct up_isr_info_s isr_ch2infotable1[mB_CH2_NSLOTS] = {0};
	static volatile struct up_isr_info_s *isr_ch2infotable_ptr = isr_ch2infotable0;
#endif
#if CONFIG_MESSBUS_USE_CH3
	static struct up_isr_info_s isr_ch3infotable0[mB_CH3_NSLOTS] = {0};
	static struct up_isr_info_s isr_ch3infotable1[mB_CH3_NSLOTS] = {0};
	static volatile struct up_isr_info_s *isr_ch3infotable_ptr = isr_ch3infotable0;
#endif
#if CONFIG_MESSBUS_USE_CH4
	static struct up_isr_info_s isr_ch4infotable0[mB_CH4_NSLOTS] = {0};
	static struct up_isr_info_s isr_ch4infotable1[mB_CH4_NSLOTS] = {0};
	static volatile struct up_isr_info_s *isr_ch4infotable_ptr = isr_ch4infotable0;
#endif

static struct up_isr_info_container_s isr_infocontainer0 =
{
#if CONFIG_MESSBUS_USE_CH1
	.info1 = isr_ch1infotable0,
#endif
#if CONFIG_MESSBUS_USE_CH2
	.info2 = isr_ch2infotable0,
#endif
#if CONFIG_MESSBUS_USE_CH3
	.info3 = isr_ch3infotable0,
#endif
#if CONFIG_MESSBUS_USE_CH4
	.info4 = isr_ch4infotable0
#endif
};

static struct up_isr_info_container_s isr_infocontainer1 =
{
#if CONFIG_MESSBUS_USE_CH1
	.info1 = isr_ch1infotable1,
#endif
#if CONFIG_MESSBUS_USE_CH2
	.info2 = isr_ch2infotable1,
#endif
#if CONFIG_MESSBUS_USE_CH3
	.info3 = isr_ch3infotable1,
#endif
#if CONFIG_MESSBUS_USE_CH4
	.info4 = isr_ch4infotable1
#endif
};

/* Arch-specific instance of messBUS_dev_t, which will be attached to the
 * upper half driver. This struct is used to bind the lower half to its
 * upper half and to share data between them.
 */
static messBUS_dev_t g_messBUSpriv =
{
	/* This variable indicates whether the device has already been opened
	 * or not. Default is closed (0).
	 */
	.open = 0,

	/* This variable indicates whether the driver is running or paused.
	 * A user always needs to open the device first and attach a slotlist
	 * via ioctl before he can actually start the driver. Default is
	 * paused (0).
	 */
	.running = 0,

	/* This variable is incremented by the interrupt handler after each
	 * successful SYNC. Higher logic can poll for this variable to become
	 * non-zero when it waits for the next timeslice. If the value of this
	 * variable is > 1 higher logic can recognise that it missed x time-
	 * slices.
	 */
	.sync_count = 0,

	/* This variable indicates whether the interrupt handler currently
	 * points to tables with index 0 or 1. It is modified by the interrupt
	 * handler. The interrupt handler only changes tables when there are
	 * new tables available. The unused tables can be modified by higher
	 * logic.
	 */
	.active_table = 0,

	/* This variable indicates that new tables are available. It is set by
	 * higher logic and cleared by the interrupt handler after the current
	 * time slice. If set to one by higher logic the interrupt handler will
	 * point to the modified tables after the current time slice.
	 */
	.new_table_avail = 0,

	/* Let the upper half know where to find the tables for the interrupt
	 * routine. This information is needed when a new slotlist is attached
	 * to the upper half driver.
	 */
	.isr_actiontable0 = isr_actiontable0,
	.isr_actiontable1 = isr_actiontable1,
	.isr_infocontainer0 = &isr_infocontainer0,
	.isr_infocontainer1 = &isr_infocontainer1,

	/* Attach the lower half operations to the upper half */
	.ops = &g_messBUS_ops
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: baud_to_brr
 *
 * Description:
 *   This function converts a baudrate into a BRR register value. The way the
 *   conversion has to be done differs for the F4 and F7 chips. See their
 *   reference manuals for more information.
 *
 * Assumptions:
 *   - Note that the F7 chips should be clocked by 216MHz SYSCLK, which has
 *     to be configured in board.h respectively.
 *
 *   - This implementation requires that oversampling by 8 is configured.
 *     Oversampling by 16 is not supported, because messBUS' relatively high
 *     baudrates can be represented way more accurately when using over-
 *     sampling by 8 in the very most cases.
 *
 ****************************************************************************/

#if defined(CONFIG_ARCH_CHIP_STM32F7)
static uint16_t up_baud_to_brr (uint32_t baud, uint32_t fck)
{
	/* As we oversample by 8, the equation is:
	 *
	 *   baud      = 2 * fCK / usartdiv8
	 *   usartdiv8 = 2 * fCK / baud
	 *
	 * fCK is the 216 Mhz SYSCLK, which allows very high baudrates. Make
	 * sure SYSCLK is selected as clock source in board.h.
	 *
	 * As we oversample by 8, magical bit shifting operation is required for
	 * bits 0 to 3 in order to obtain brr register value from usartdiv8.
	 *
	 * Baud/2 is added to fCK to support rounding.
	 */
	//uint16_t usartdiv8 = (uint16_t) (((STM32_SYSCLK_FREQUENCY << 1) + (baud >> 1)) / baud);
	uint16_t usartdiv8 = (uint16_t) (((fck << 1) + (baud >> 1)) / baud);
	uint16_t brr = ((usartdiv8 & 0xfff0) | ((usartdiv8 & 0x000f) >> 1));

	return brr;
}
#elif defined(CONFIG_STM32_STM32F4XXX)
static uint16_t up_baud_to_brr (uint32_t baud, uint32_t fck)
{
	/* This second implementation is for U[S]ARTs that support fractional
	 * dividers, such as the F4's USARTS.
	 *
	 * Configure the USART Baud Rate.  The baud rate for the receiver and
	 * transmitter (Rx and Tx) are both set to the same value as programmed
	 * in the Mantissa and Fraction values of USARTDIV. In case of
	 * oversampling by 8 the equations are:
	 *
	 *   baud     = fCK / (8 * usartdiv)
	 *   usartdiv = fCK / (8 * baud)
	 *
	 * Where fCK is the input clock to the peripheral (PCLK1 for USART2, 3, 4, 5
	 * or PCLK2 for USART1 and USART6)
	 *
	 * First calculate (NOTE: all stand baud values are even so dividing by two
	 * does not lose precision):
	 *
	 *   usartdiv32 = 32 * usartdiv = fCK / (baud/2)
	 */
	uint16_t usartdiv32 = (uint16_t) (fck / (baud >> 1));

	/* The mantissa part is then */
	uint16_t mantissa = (usartdiv32 >> 4);

	/* The fractional remainder (with rounding) */
	uint16_t fraction = ((usartdiv32 - (mantissa << 4) + 1) >> 1);

	/* Place both values in the brr which will be returned */
	uint16_t brr = (mantissa << USART_BRR_MANT_SHIFT);
	brr |= (fraction << USART_BRR_FRAC_SHIFT);

	return brr;
}
#endif


/****************************************************************************
 * Name: up_convert_slotlist
 *
 * Description:
 *   This function converts a slotlist container into both a action- and
 *   infotable, which the interrupt handler will use to perform the desired
 *   slots.
 *
 * Input arguments:
 *   container - A reference to the slotlistcontainer
 *   actiontable - A reference to a preallocated actiontable
 *   infocontainer - A reference to a preallocated infocontainer
 *
 ****************************************************************************/
#if CONFIG_MESSBUS_NCHANNELS == 1
static void up_convert_slotlist(void *container, void *actiontable, void *infocontainer)
{
	uint8_t remaining_slots = ((struct slotlists_container_s *)container)->mB_CHx_SLOTLIST->n_slots;
	struct slot_s *slot_ptr = ((struct slotlists_container_s *)container)->mB_CHx_SLOTLIST->slot;
//	struct up_isr_action_s *action_ptr = actiontable;
//	struct up_isr_info_container_s *infocontainer_ptr = infocontainer;
//	struct up_isr_info_s *info_ptr = infocontainer_ptr->mB_CHx_INFOTABLE;
	struct up_isr_action_s *action_ptr = (struct up_isr_action_s *) actiontable;
	struct up_isr_info_container_s *infocontainer_ptr = (struct up_isr_info_container_s *) infocontainer;
	struct up_isr_info_s *info_ptr = infocontainer_ptr->mB_CHx_INFOTABLE;
	uint16_t nom_ticks;
	int32_t real_ticks;

	while (remaining_slots > 0)
	{
		if (slot_ptr->read_write == MESSBUS_TX)
		{
			/* Tx operation has three actions:
			 *
			 * - CHx_TX_DE (Data Enable)
			 * - CHx_TX_START
			 * - CHx_TX_END
			 *
			 * So we create three timed actions and a single info structure for
			 * this slot. We start with the first action:
			 */
			action_ptr->op_mode = mB_CHx_TX_DE;
			nom_ticks = ((slot_ptr->t_start) * 4) + mB_OFFSET_TICKS;
			real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
			action_ptr->ccr3 = (uint16_t) real_ticks;
			action_ptr++;

			/* Schedule second action two ticks (0.5us) after first action. */
			action_ptr->op_mode = mB_CHx_TX_START;
			nom_ticks = ((slot_ptr->t_start) * 4) + mB_OFFSET_TICKS + 2;
			real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
			action_ptr->ccr3 = (uint16_t) real_ticks;
			action_ptr++;

			/* Third action: */
			action_ptr->op_mode = mB_CHx_TX_END;
			nom_ticks = ((slot_ptr->t_end) * 4) + mB_OFFSET_TICKS;
			real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
			action_ptr->ccr3 = (uint16_t) real_ticks;
			action_ptr++;
		}
		else
		{
			/* Tx operation has two actions:
			 *
			 * - CHx_RX_START
			 * - CHx_RX_END
			 *
			 * So we create two timed actions and a single info structure for
			 * this slot. We start with the first action:
			 */

			/*
			 * messBUS node need to begin offset us before rx slot with its preparation
			 * to ensure that it is ready to read data an slot begin
			 */
			uint16_t t_start_corr = slot_ptr->t_start + mB_RX_OFFSET_US;

			action_ptr->op_mode = mB_CHx_RX_START;
			nom_ticks = ((t_start_corr) * 4) + mB_OFFSET_TICKS;
			real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
			action_ptr->ccr3 = (uint16_t) real_ticks;
			action_ptr++;

			/* Second action: */
			action_ptr->op_mode = mB_CHx_RX_END;
			nom_ticks = ((slot_ptr->t_end) * 4) + mB_OFFSET_TICKS;
			real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
			action_ptr->ccr3 = (uint16_t) real_ticks;
			action_ptr++;
		}

		/* Finally the info for this slot, which is the same regardless of
		 * TX or RX operation:
		 */
		info_ptr->brr = up_baud_to_brr(slot_ptr->baud, STM32_SYSCLK_FREQUENCY);
		info_ptr->buffer = slot_ptr->buffer;
		info_ptr->buf_length_ptr = &slot_ptr->buf_length;
		info_ptr->buf_length = slot_ptr->buf_length;

		/* This slot is done now. */
		remaining_slots--;

		/* If there are still remaining slots increment the pointers */
		if (remaining_slots > 0)
		{
			info_ptr++;
			slot_ptr++;
		}
	}

	/* Insert last action (case 2) here. This action will disable
	 * timer 3 2 us before the trigger will reenable it.
	 */
	action_ptr->op_mode = 2;
	nom_ticks = mB_TIM3_CCR3_2;
	real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
	action_ptr->ccr3 = (uint16_t) real_ticks;
}
#else // CONFIG_MESSBUS_NCHANNELS > 1
static void up_convert_slotlist(void *container, void *actiontable, void *infocontainer)
{
	struct up_isr_action_s *action_ptr;
#if CONFIG_MESSBUS_USE_CH1
	struct up_isr_info_s *ch1_info_ptr;
#endif
#if CONFIG_MESSBUS_USE_CH2
	struct up_isr_info_s *ch2_info_ptr;
#endif
#if CONFIG_MESSBUS_USE_CH3
	struct up_isr_info_s *ch3_info_ptr;
#endif
#if CONFIG_MESSBUS_USE_CH4
	struct up_isr_info_s *ch4_info_ptr;
#endif
	uint16_t nom_ticks;
	int32_t real_ticks;
	uint8_t next_channel = 0;
	uint16_t ccr3_to_beat = 0xffff;

	if (g_messBUSpriv.running)
	{
		if (g_messBUSpriv.active_table)
		{
			action_ptr = isr_actiontable0;
			#if CONFIG_MESSBUS_USE_CH1
			ch1_info_ptr = isr_ch1infotable0;
			#endif
			#if CONFIG_MESSBUS_USE_CH2
			ch2_info_ptr = isr_ch2infotable0;
			#endif
			#if CONFIG_MESSBUS_USE_CH3
			ch3_info_ptr = isr_ch3infotable0;
			#endif
			#if CONFIG_MESSBUS_USE_CH4
			ch4_info_ptr = isr_ch4infotable0;
			#endif
		}
		else
		{
			action_ptr = isr_actiontable1;
			#if CONFIG_MESSBUS_USE_CH1
			ch1_info_ptr = isr_ch1infotable1;
			#endif
			#if CONFIG_MESSBUS_USE_CH2
			ch2_info_ptr = isr_ch2infotable1;
			#endif
			#if CONFIG_MESSBUS_USE_CH3
			ch3_info_ptr = isr_ch3infotable1;
			#endif
			#if CONFIG_MESSBUS_USE_CH4
			ch4_info_ptr = isr_ch4infotable1;
			#endif
		}
	}
	else
	{
		action_ptr = isr_actiontable0;
		#if CONFIG_MESSBUS_USE_CH1
		ch1_info_ptr = isr_ch1infotable0;
		#endif
		#if CONFIG_MESSBUS_USE_CH2
		ch2_info_ptr = isr_ch2infotable0;
		#endif
		#if CONFIG_MESSBUS_USE_CH3
		ch3_info_ptr = isr_ch3infotable0;
		#endif
		#if CONFIG_MESSBUS_USE_CH4
		ch4_info_ptr = isr_ch4infotable0;
		#endif
	}
//	struct up_isr_action_s *action_ptr = actiontable;
//	struct up_isr_info_container_s *infocontainer_ptr = infocontainer;
//	uint16_t nom_ticks;
//	int32_t real_ticks;
//	uint8_t next_channel = 0;
//	uint16_t ccr3_to_beat = 0xffff;

#if CONFIG_MESSBUS_USE_CH1
	uint8_t ch1_remaining_slots = ((struct slotlists_container_s *)container)->slotlist1->n_slots;
	struct slot_s *ch1_slot_ptr = ((struct slotlists_container_s *)container)->slotlist1->slot;
//	struct up_isr_info_s *ch1_info_ptr = infocontainer_ptr->info1;
#else
	uint8_t ch1_remaining_slots = 0;
#endif
#if CONFIG_MESSBUS_USE_CH2
	uint8_t ch2_remaining_slots = ((struct slotlists_container_s *)container)->slotlist2->n_slots;
	struct slot_s *ch2_slot_ptr = ((struct slotlists_container_s *)container)->slotlist2->slot;
//	struct up_isr_info_s *ch2_info_ptr = infocontainer_ptr->info2;
#else
	uint8_t ch2_remaining_slots = 0;
#endif
#if CONFIG_MESSBUS_USE_CH3
	uint8_t ch3_remaining_slots = ((struct slotlists_container_s *)container)->slotlist3->n_slots;
	struct slot_s *ch3_slot_ptr = ((struct slotlists_container_s *)container)->slotlist3->slot;
//	struct up_isr_info_s *ch3_info_ptr = infocontainer_ptr->info3;
#else
	uint8_t ch3_remaining_slots = 0;
#endif
#if CONFIG_MESSBUS_USE_CH4
	uint8_t ch4_remaining_slots = ((struct slotlists_container_s *)container)->slotlist4->n_slots;
	struct slot_s *ch4_slot_ptr = ((struct slotlists_container_s *)container)->slotlist4->slot;
//	struct up_isr_info_s *ch4_info_ptr = infocontainer_ptr->info4;
#else
	uint8_t ch4_remaining_slots = 0;
#endif

	/* Already get the first pending actions,
	 * which should all be TX for the master.
	 */
#if CONFIG_MESSBUS_USE_CH1
	struct up_isr_action_s ch1_pending_action;
	ch1_pending_action.op_mode = mB_CH1_TX_DE;
	nom_ticks = ((ch1_slot_ptr->t_start) * 4) + mB_OFFSET_TICKS;
	real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
	ch1_pending_action.ccr3 = (uint16_t) real_ticks;
#endif
#if CONFIG_MESSBUS_USE_CH2
	struct up_isr_action_s ch2_pending_action;
	ch2_pending_action.op_mode = mB_CH2_TX_DE;
	nom_ticks = ((ch2_slot_ptr->t_start) * 4) + mB_OFFSET_TICKS;
	real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
	ch2_pending_action.ccr3 = (uint16_t) real_ticks;
#endif
#if CONFIG_MESSBUS_USE_CH3
	struct up_isr_action_s ch3_pending_action;
	ch3_pending_action.op_mode = mB_CH3_TX_DE;
	nom_ticks = ((ch3_slot_ptr->t_start) * 4) + mB_OFFSET_TICKS;
	real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
	ch3_pending_action.ccr3 = (uint16_t) real_ticks;
#endif
#if CONFIG_MESSBUS_USE_CH4
	struct up_isr_action_s ch4_pending_action;
	ch4_pending_action.op_mode = mB_CH4_TX_DE;
	nom_ticks = ((ch4_slot_ptr->t_start) * 4) + mB_OFFSET_TICKS;
	real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
	ch4_pending_action.ccr3 = (uint16_t) real_ticks;
#endif

	/* Loop as long as there are any slots remaining */
	while (ch1_remaining_slots || ch2_remaining_slots || ch3_remaining_slots || ch4_remaining_slots)
	{

		/* Which channel is next? */
#if CONFIG_MESSBUS_USE_CH1
		if(ch1_remaining_slots > 0)
		{
			if (ch1_pending_action.ccr3 < ccr3_to_beat)
			{
			ccr3_to_beat = ch1_pending_action.ccr3;
			next_channel = 1;
			}
		}
#endif
#if CONFIG_MESSBUS_USE_CH2
		if(ch2_remaining_slots > 0)
		{
			if (ch2_pending_action.ccr3 < ccr3_to_beat)
			{
			ccr3_to_beat = ch2_pending_action.ccr3;
			next_channel = 2;
			}
		}
#endif
#if CONFIG_MESSBUS_USE_CH3
		if(ch3_remaining_slots > 0)
		{
			if (ch3_pending_action.ccr3 < ccr3_to_beat)
			{
			ccr3_to_beat = ch3_pending_action.ccr3;
			next_channel = 3;
			}
		}
#endif
#if CONFIG_MESSBUS_USE_CH4
		if(ch4_remaining_slots > 0)
		{
			if (ch4_pending_action.ccr3 < ccr3_to_beat)
			{
			ccr3_to_beat = ch4_pending_action.ccr3;
			next_channel = 4;
			}
		}
#endif

		/* Schedule the next action */
		switch(next_channel)
		{
#if CONFIG_MESSBUS_USE_CH1
		case 1:
		action_ptr->op_mode = ch1_pending_action.op_mode;
		action_ptr->ccr3 = ch1_pending_action.ccr3;
		action_ptr++;

		/* Get the new pending action for this channel */
		switch(ch1_pending_action.op_mode)
			{
			case mB_CH1_TX_DE:
				ch1_pending_action.op_mode = mB_CH1_TX_START;
				nom_ticks = ((ch1_slot_ptr->t_start) * 4) + mB_OFFSET_TICKS + 2;
				real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
				ch1_pending_action.ccr3 = (uint16_t) real_ticks;
				break;

			case mB_CH1_TX_START:
				ch1_pending_action.op_mode = mB_CH1_TX_END;
				nom_ticks = ((ch1_slot_ptr->t_end) * 4) + mB_OFFSET_TICKS;
				real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
				ch1_pending_action.ccr3 = (uint16_t) real_ticks;
				break;

			case mB_CH1_RX_START:
				ch1_pending_action.op_mode = mB_CH1_RX_END;
				nom_ticks = ((ch1_slot_ptr->t_end) * 4) + mB_OFFSET_TICKS;
				real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
				ch1_pending_action.ccr3 = (uint16_t) real_ticks;
				break;

			case mB_CH1_TX_END:
			case mB_CH1_RX_END:

				/* Parse the slot info */
				ch1_info_ptr->brr = up_baud_to_brr(ch1_slot_ptr->baud, STM32_SYSCLK_FREQUENCY);
				ch1_info_ptr->buffer = ch1_slot_ptr->buffer;
				ch1_info_ptr->buf_length_ptr = &ch1_slot_ptr->buf_length;
				ch1_info_ptr->buf_length = ch1_slot_ptr->buf_length;

				/* This slot is done now. */
				ch1_remaining_slots--;

				if (ch1_remaining_slots > 0)
				{
					ch1_info_ptr++;
					ch1_slot_ptr++;

					/* Is the next slot TX or RX? */
					if (ch1_slot_ptr->read_write == MESSBUS_TX)
					{
						ch1_pending_action.op_mode = mB_CH1_TX_DE;
					}
					else
					{
						ch1_pending_action.op_mode = mB_CH1_RX_START;
					}

					/* Also get the time information */
					nom_ticks = ((ch1_slot_ptr->t_start) * 4) + mB_OFFSET_TICKS;
					real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
					ch1_pending_action.ccr3 = (uint16_t) real_ticks;
				}
				else
				{
					ch1_pending_action.ccr3 = 0xffff;
				}

				break;
			}

			/* The current channel will be the next, unless another
			 * channel can underbid ccr3_to_beat.
			 */

			/*
			 * messBUS node need to begin offset us before rx slot with its preparation
			 * to ensure that it is ready to read data an slot begin
			 */
			uint16_t t_start_corr = slot_ptr->t_start + mB_RX_OFFSET_US;

			action_ptr->op_mode = mB_CH4_RX_START;
			nom_ticks = ((t_start_corr) * 4) + mB_OFFSET_TICKS;
			real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
			action_ptr->ccr3 = (uint16_t) real_ticks;
			action_ptr++;

			break;
#endif // CONFIG_MESSBUS_USE_CH1
#if CONFIG_MESSBUS_USE_CH2
		case 2:
		action_ptr->op_mode = ch2_pending_action.op_mode;
		action_ptr->ccr3 = ch2_pending_action.ccr3;
		action_ptr++;

		/* Get the new pending action for this channel */
		switch(ch2_pending_action.op_mode)
			{
			case mB_CH2_TX_DE:
				ch2_pending_action.op_mode = mB_CH2_TX_START;
				nom_ticks = ((ch2_slot_ptr->t_start) * 4) + mB_OFFSET_TICKS + 2;
				real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
				ch2_pending_action.ccr3 = (uint16_t) real_ticks;
				break;

			case mB_CH2_TX_START:
				ch2_pending_action.op_mode = mB_CH2_TX_END;
				nom_ticks = ((ch2_slot_ptr->t_end) * 4) + mB_OFFSET_TICKS;
				real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
				ch2_pending_action.ccr3 = (uint16_t) real_ticks;
				break;

			case mB_CH2_RX_START:
				ch2_pending_action.op_mode = mB_CH2_RX_END;
				nom_ticks = ((ch2_slot_ptr->t_end) * 4) + mB_OFFSET_TICKS;
				real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
				ch2_pending_action.ccr3 = (uint16_t) real_ticks;
				break;

			case mB_CH2_TX_END:
			case mB_CH2_RX_END:

				/* Parse the slot info */
				ch2_info_ptr->brr = up_baud_to_brr(ch2_slot_ptr->baud, STM32_SYSCLK_FREQUENCY);
				ch2_info_ptr->buffer = ch2_slot_ptr->buffer;
				ch2_info_ptr->buf_length_ptr = &ch2_slot_ptr->buf_length;
				ch2_info_ptr->buf_length = ch2_slot_ptr->buf_length;

				/* This slot is done now. */
				ch2_remaining_slots--;

				if (ch2_remaining_slots > 0)
				{
					ch2_info_ptr++;
					ch2_slot_ptr++;

					/* Is the next slot TX or RX? */
					if (ch2_slot_ptr->read_write == MESSBUS_TX)
					{
						ch2_pending_action.op_mode = mB_CH2_TX_DE;
					}
					else
					{
						ch2_pending_action.op_mode = mB_CH2_RX_START;
					}

					/* Also get the time information */
					nom_ticks = ((ch2_slot_ptr->t_start) * 4) + mB_OFFSET_TICKS;
					real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
					ch2_pending_action.ccr3 = (uint16_t) real_ticks;
				}
				else
				{
					ch2_pending_action.ccr3 = 0xffff;
				}

				break;
			}

			/* The current channel will be the next, unless another
			 * channel can underbid ccr3_to_beat.
			 */
			ccr3_to_beat = ch2_pending_action.ccr3;
			next_channel = 2;

			break;
#endif // CONFIG_MESSBUS_USE_CH2
#if CONFIG_MESSBUS_USE_CH3
		case 3:
		action_ptr->op_mode = ch3_pending_action.op_mode;
		action_ptr->ccr3 = ch3_pending_action.ccr3;
		action_ptr++;

		/* Get the new pending action for this channel */
		switch(ch3_pending_action.op_mode)
			{
			case mB_CH3_TX_DE:
				ch3_pending_action.op_mode = mB_CH3_TX_START;
				nom_ticks = ((ch3_slot_ptr->t_start) * 4) + mB_OFFSET_TICKS + 2;
				real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
				ch3_pending_action.ccr3 = (uint16_t) real_ticks;
				break;

			case mB_CH3_TX_START:
				ch3_pending_action.op_mode = mB_CH3_TX_END;
				nom_ticks = ((ch3_slot_ptr->t_end) * 4) + mB_OFFSET_TICKS;
				real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
				ch3_pending_action.ccr3 = (uint16_t) real_ticks;
				break;

			case mB_CH3_RX_START:
				ch3_pending_action.op_mode = mB_CH3_RX_END;
				nom_ticks = ((ch3_slot_ptr->t_end) * 4) + mB_OFFSET_TICKS;
				real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
				ch3_pending_action.ccr3 = (uint16_t) real_ticks;
				break;

			case mB_CH3_TX_END:
			case mB_CH3_RX_END:

				/* Parse the slot info */
				ch3_info_ptr->brr = up_baud_to_brr(ch3_slot_ptr->baud, STM32_SYSCLK_FREQUENCY);
				ch3_info_ptr->buffer = ch3_slot_ptr->buffer;
				ch3_info_ptr->buf_length_ptr = &ch3_slot_ptr->buf_length;
				ch3_info_ptr->buf_length = ch3_slot_ptr->buf_length;

				/* This slot is done now. */
				ch3_remaining_slots--;

				if (ch3_remaining_slots > 0)
				{
					ch3_info_ptr++;
					ch3_slot_ptr++;

					/* Is the next slot TX or RX? */
					if (ch3_slot_ptr->read_write == MESSBUS_TX)
					{
						ch3_pending_action.op_mode = mB_CH3_TX_DE;
					}
					else
					{
						ch3_pending_action.op_mode = mB_CH3_RX_START;
					}

					/* Also get the time information */
					nom_ticks = ((ch3_slot_ptr->t_start) * 4) + mB_OFFSET_TICKS;
					real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
					ch3_pending_action.ccr3 = (uint16_t) real_ticks;
				}
				else
				{
					ch3_pending_action.ccr3 = 0xffff;
				}

				break;
			}

			/* The current channel will be the next, unless another
			 * channel can underbid ccr3_to_beat.
			 */
			ccr3_to_beat = ch3_pending_action.ccr3;
			next_channel = 3;

			break;
#endif // CONFIG_MESSBUS_USE_CH3
#if CONFIG_MESSBUS_USE_CH4
			case 4:
			action_ptr->op_mode = ch4_pending_action.op_mode;
			action_ptr->ccr3 = ch4_pending_action.ccr3;
			action_ptr++;

			/* Get the new pending action for this channel */
			switch(ch4_pending_action.op_mode)
				{
				case mB_CH4_TX_DE:
					ch4_pending_action.op_mode = mB_CH4_TX_START;
					nom_ticks = ((ch4_slot_ptr->t_start) * 4) + mB_OFFSET_TICKS + 2;
					real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
					ch4_pending_action.ccr3 = (uint16_t) real_ticks;
					break;

				case mB_CH4_TX_START:
					ch4_pending_action.op_mode = mB_CH4_TX_END;
					nom_ticks = ((ch4_slot_ptr->t_end) * 4) + mB_OFFSET_TICKS;
					real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
					ch4_pending_action.ccr3 = (uint16_t) real_ticks;
					break;

				case mB_CH4_RX_START:
					ch4_pending_action.op_mode = mB_CH4_RX_END;
					nom_ticks = ((ch4_slot_ptr->t_end) * 4) + mB_OFFSET_TICKS;
					real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
					ch4_pending_action.ccr3 = (uint16_t) real_ticks;
					break;

				case mB_CH4_TX_END:
				case mB_CH4_RX_END:

					/* Parse the slot info */
					ch4_info_ptr->brr = up_baud_to_brr(ch4_slot_ptr->baud, STM32_SYSCLK_FREQUENCY);
					ch4_info_ptr->buffer = ch4_slot_ptr->buffer;
					ch4_info_ptr->buf_length_ptr = &ch4_slot_ptr->buf_length;
					ch4_info_ptr->buf_length = ch4_slot_ptr->buf_length;

					/* This slot is done now. */
					ch4_remaining_slots--;

					if (ch4_remaining_slots > 0)
					{
						ch4_info_ptr++;
						ch4_slot_ptr++;

						/* Is the next slot TX or RX? */
						if (ch4_slot_ptr->read_write == MESSBUS_TX)
						{
							ch4_pending_action.op_mode = mB_CH4_TX_DE;
						}
						else
						{
							ch4_pending_action.op_mode = mB_CH4_RX_START;
						}

						/* Also get the time information */
						nom_ticks = ((ch4_slot_ptr->t_start) * 4) + mB_OFFSET_TICKS;
						real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
						ch4_pending_action.ccr3 = (uint16_t) real_ticks;
					}
					else
					{
						ch4_pending_action.ccr3 = 0xffff;
					}

					break;
				}

				/* The current channel will be the next, unless another
				 * channel can underbid ccr3_to_beat.
				 */
				ccr3_to_beat = ch4_pending_action.ccr3;
				next_channel = 4;

				break;
			}
#endif //CONFIG_MESSBUS_USE_CH4
	}

	/* Insert last action (case 2) here. This action will disable
	 * timer 3 2 us before the trigger will reenable it.
	 */
	action_ptr->op_mode = 2;
	nom_ticks = mB_TIM3_CCR3_2;
	real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR);
	action_ptr->ccr3 = (uint16_t) real_ticks;

}
#endif // CONFIG_MESSBUS_NCHANNELS

/****************************************************************************
 * Name: tim3_handler
 *
 * Description:
 *   This is the handler for the high speed TIM3 interrupt.
 *
 ****************************************************************************/

static void tim3_handler(void)
{
#if mB_ISR_DEBUG_GPIO
	/* Set Debug-GPIO */
	setbit32(STM32_GPIOD_BSRR, GPIO_DEBUG_ISR_ON);
#endif

	/* State machine */
	NEXT_ACTION: switch(g_op_mode)
	{

	/* First init action (before SYNC) */
	case 0:

		/* Acknowledge the trigger interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered! */
		clearbit16(STM32_TIM3_SR, GTIM_SR_TIF);

#if CONFIG_MESSBUS_USE_CH1
		/* Set USART1 Tx pin to HiZ input for SYNC depending on configuration in board.h */
		clearbit32(mB_USART1TX_MODER, mB_USART1TX_HI_Z);

		/* Activate timer output pin for SYNC depending on configuration in board.h */
		setbit32(mB_TIM1_CH1OUT_MODER, mB_TIM1_CH1OUT_ACTIVE);

		/* Enable the Data Enable pin */
		setbit32(mB_CH1_DE_BSRR, mB_CH1_DE_ON);
#endif
#if CONFIG_MESSBUS_USE_CH2
		/* Set USART2 Tx pin to HiZ input for SYNC depending on configuration in board.h */
		clearbit32(mB_USART2TX_MODER, mB_USART2TX_HI_Z);

		/* Activate timer output pin for SYNC depending on configuration in board.h */
		setbit32(mB_TIM1_CH2OUT_MODER, mB_TIM1_CH2OUT_ACTIVE);

		/* Enable the Data Enable pin */
		setbit32(mB_CH2_DE_BSRR, mB_CH2_DE_ON);
#endif
#if CONFIG_MESSBUS_USE_CH3
		/* Set USART3 Tx pin to HiZ input for SYNC depending on configuration in board.h */
		clearbit32(mB_USART3TX_MODER, mB_USART3TX_HI_Z);

		/* Activate timer output pin for SYNC depending on configuration in board.h */
		setbit32(mB_TIM1_CH3OUT_MODER, mB_TIM1_CH3OUT_ACTIVE);

		/* Enable the Data Enable pin */
		setbit32(mB_CH3_DE_BSRR, mB_CH3_DE_ON);
#endif
#if CONFIG_MESSBUS_USE_CH4
		/* Set USART4 Tx pin to HiZ input for SYNC depending on configuration in board.h */
		clearbit32(mB_USART4TX_MODER, mB_USART4TX_HI_Z);

		/* Activate timer output pin for SYNC depending on configuration in board.h */
		setbit32(mB_TIM1_CH4OUT_MODER, mB_TIM1_CH4OUT_ACTIVE);

		/* Enable the Data Enable pin */
		setbit32(mB_CH4_DE_BSRR, mB_CH4_DE_ON);
#endif

		/* Are there new tables available from higher logic? If yes switch tables
		 * and reset the flag.
		 */
		if(g_messBUSpriv.new_table_avail)
		{
			g_messBUSpriv.new_table_avail = 0;
			if (g_messBUSpriv.active_table)
			{
				g_messBUSpriv.active_table = 0;
			}
			else
				g_messBUSpriv.active_table = 1;
		}

		/* Choose tables for next timeslice according to updated g_isr_active_tables flag */
		if (g_messBUSpriv.active_table)
		{
			isr_actiontable_ptr = isr_actiontable1;
#if CONFIG_MESSBUS_USE_CH1
			isr_ch1infotable_ptr = isr_ch1infotable1;
#endif
#if CONFIG_MESSBUS_USE_CH2
			isr_ch2infotable_ptr = isr_ch2infotable1;
#endif
#if CONFIG_MESSBUS_USE_CH3
			isr_ch3infotable_ptr = isr_ch3infotable1;
#endif
#if CONFIG_MESSBUS_USE_CH4
			isr_ch4infotable_ptr = isr_ch4infotable1;
#endif
		}
		else
		{
			isr_actiontable_ptr = isr_actiontable0;
#if CONFIG_MESSBUS_USE_CH1
			isr_ch1infotable_ptr = isr_ch1infotable0;
#endif
#if CONFIG_MESSBUS_USE_CH2
			isr_ch2infotable_ptr = isr_ch2infotable0;
#endif
#if CONFIG_MESSBUS_USE_CH3
			isr_ch3infotable_ptr = isr_ch3infotable0;
#endif
#if CONFIG_MESSBUS_USE_CH4
			isr_ch4infotable_ptr = isr_ch4infotable0;
#endif
		}

		/* Set new CCR3 value for next capture interrupt. */
		putreg16(mB_TIM3_CCR3_1, STM32_TIM3_CCR3);

		/* Next case: Init action 1 */
		g_op_mode = 1;

		break;

	/* Second init action (after SYNC) */
	case 1:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

#if CONFIG_MESSBUS_USE_CH1
		/* Reset timer output pin to HiZ input for data transmission depending on configuration in board.h */
		clearbit32(mB_TIM1_CH1OUT_MODER, mB_TIM1_CH1OUT_HI_Z);

		/* Activate USART1 Tx pin for data transmission depending on configuration in board.h */
		setbit32(mB_USART1TX_MODER, mB_USART1TX_ACTIVE);
#endif
#if CONFIG_MESSBUS_USE_CH2
		/* Reset timer output pin to HiZ input for data transmission depending on configuration in board.h */
		clearbit32(mB_TIM1_CH2OUT_MODER, mB_TIM1_CH2OUT_HI_Z);

		/* Activate USART2 Tx pin for data transmission depending on configuration in board.h */
		setbit32(mB_USART2TX_MODER, mB_USART2TX_ACTIVE);
#endif
#if CONFIG_MESSBUS_USE_CH3
		/* Reset timer output pin to HiZ input for data transmission depending on configuration in board.h */
		clearbit32(mB_TIM1_CH3OUT_MODER, mB_TIM1_CH3OUT_HI_Z);

		/* Activate USART3 Tx pin for data transmission depending on configuration in board.h */
		setbit32(mB_USART3TX_MODER, mB_USART3TX_ACTIVE);
#endif
#if CONFIG_MESSBUS_USE_CH4
		/* Reset timer output pin to HiZ input for data transmission depending on configuration in board.h */
		clearbit32(mB_TIM1_CH4OUT_MODER, mB_TIM1_CH4OUT_HI_Z);

		/* Activate USART4 Tx pin for data transmission depending on configuration in board.h */
		setbit32(mB_USART4TX_MODER, mB_USART4TX_ACTIVE);
#endif

		/* Leave the Data Enable pin(s) high. The next action(s) will
		 * be TX anyway (Mastermessage).
		 */

		/* Set new CCR3 value for next capture interrupt. */
		putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

		/* Next case: First action */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Finally increment relevant table pointers */
		isr_actiontable_ptr++;

		/* Signal that there was another SYNC */
		g_messBUSpriv.sync_count++;

		break;

	/* Last action */
	case 2:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Disable and reset timer 3. Timer 3 will now wait for trigger from timer 2 */
		clearbit16(STM32_TIM3_CR1, GTIM_CR1_CEN);
		putreg16(0x0000 , STM32_TIM3_CNT);

		/* Next case: First init action */
		g_op_mode = 0;

		break;

#if CONFIG_MESSBUS_USE_CH1
	/* Prepare Rx operation */
	case mB_CH1_RX_START:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Rx start specific operations now...
		 * Set baudrate first:
		 */
		putreg32((uint32_t) isr_ch1infotable_ptr->brr, STM32_USART1_BRR);

		/* Prepare and enable the DMA stream */
		putreg32((uint32_t) isr_ch1infotable_ptr->buffer, mB_USART1RX_DMA_SxMOAR);	// Set the memory address
		putreg32((uint32_t) 0xffff, mB_USART1RX_DMA_SxNDTR);						// Set the NDTR to max 0xffff
		setbit32(mB_USART1RX_DMA_xIFCR, mB_USART1RX_DMA_INT_SxMASK);				// Clear pending flags for safety reasons
		setbit32(mB_USART1RX_DMA_SxCR, DMA_SCR_EN);									// Enable the stream

		/* Enable the Receive Enable bit at the USART */
		setbit32(STM32_USART1_CR1, USART_CR1_RE);

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Stop Rx operation */
	case mB_CH1_RX_END:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Rx end specific operations now...
		 * Disable the Receive Enable bit at the USART first. Incomplete transfers will be aborted,
		 * so after enabling the receiver again it will start searching for a start bit again.
		 */
		clearbit32(STM32_USART1_CR1, USART_CR1_RE);

		/* Disable the DMA stream */
		clearbit32(mB_USART1RX_DMA_SxCR, DMA_SCR_EN);

		/* Write the number of received bytes directly into the slotlist (as feedback) */
		*isr_ch1infotable_ptr->buf_length_ptr = 0xffff - ((uint16_t) getreg32(mB_USART1RX_DMA_SxNDTR));

		/* Increment infotable pointer as we have completed this RX slot now. */
		isr_ch1infotable_ptr++;

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Tx data enable operation */
	case mB_CH1_TX_DE:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx data enable specific operations now...
		 * Set the Data Enable pin:
		 */
		setbit32(mB_CH1_DE_BSRR, mB_CH1_DE_ON);

		/* Set baudrate */
		putreg32((uint32_t) isr_ch1infotable_ptr->brr, STM32_USART1_BRR);

		/* Already prepare the DMA stream */
		putreg32((uint32_t) isr_ch1infotable_ptr->buffer, mB_USART1TX_DMA_SxMOAR);				// Set the memory address
		putreg32((uint32_t) isr_ch1infotable_ptr->buf_length, mB_USART1TX_DMA_SxNDTR);			// Set the NDTR
		setbit32(mB_USART1TX_DMA_xIFCR, mB_USART1TX_DMA_INT_SxMASK);						// Clear pending flags for safety reasons

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
				}

		break;

	/* Tx start operation */
	case mB_CH1_TX_START:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx start specific operations now...
		 * Enable the previously prepared DMA stream now:
		 */
		setbit32(mB_USART1TX_DMA_SxCR, DMA_SCR_EN);

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Tx end operation */
	case mB_CH1_TX_END:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx end specific operations now...
		 * Reset the Data Enable pin first:
		 */
		setbit32(mB_CH1_DE_BSRR, mB_CH1_DE_OFF);

		/* Force the DMA stream to disable itself, which should already have happened after
		 * transmission of all bytes according to previous NDTR setting.
		 * Hence, this is only a safety feature to make sure that the next DMA transfer can
		 * start immediately and there is no pending data from the previous transfer. The
		 * disabling procedure may takes some time, as the DMA waits for the current transfer
		 * to complete before actually writing the DMA_SCR_EN bit to zero. But this is ok for
		 * our purposes, as the next transfer will not happen immediately.
		 */
		clearbit32(mB_USART1TX_DMA_SxCR, DMA_SCR_EN);

		/* Increment infotable pointer */
		isr_ch1infotable_ptr++;

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
					{
					isr_actiontable_ptr++;
					goto NEXT_ACTION;
					}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;
#endif // CONFIG_MESSBUS_USE_CH1

#if CONFIG_MESSBUS_USE_CH2
	/* Prepare Rx operation */
	case mB_CH2_RX_START:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Rx start specific operations now...
		 * Set baudrate first:
		 */
		putreg32((uint32_t) isr_ch2infotable_ptr->brr, STM32_USART2_BRR);

		/* Prepare and enable the DMA stream */
		putreg32((uint32_t) isr_ch2infotable_ptr->buffer, mB_USART2RX_DMA_SxMOAR);		// Set the memory address
		putreg32((uint32_t) 0xffff, mB_USART2RX_DMA_SxNDTR);						// Set the NDTR to max 0xffff
		setbit32(mB_USART2RX_DMA_xIFCR, mB_USART2RX_DMA_INT_SxMASK);				// Clear pending flags for safety reasons
		setbit32(mB_USART2RX_DMA_SxCR, DMA_SCR_EN);									// Enable the stream

		/* Enable the Receive Enable bit at the USART */
		setbit32(STM32_USART2_CR1, USART_CR1_RE);

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Stop Rx operation */
	case mB_CH2_RX_END:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Rx end specific operations now...
		 * Disable the Receive Enable bit at the USART first. Incomplete transfers will be aborted,
		 * so after enabling the receiver again it will start searching for a start bit again.
		 */
		clearbit32(STM32_USART2_CR1, USART_CR1_RE);

		/* Disable the DMA stream */
		clearbit32(mB_USART2RX_DMA_SxCR, DMA_SCR_EN);

		/* Write the number of received bytes directly into the slotlist (as feedback) */
		*isr_ch2infotable_ptr->buf_length_ptr = 0xffff - ((uint16_t) getreg32(mB_USART2RX_DMA_SxNDTR));

		/* Increment infotable pointer as we have completed this RX slot now. */
		isr_ch2infotable_ptr++;

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Tx data enable operation */
	case mB_CH2_TX_DE:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx data enable specific operations now...
		 * Set the Data Enable pin:
		 */
		setbit32(mB_CH2_DE_BSRR, mB_CH2_DE_ON);

		/* Set baudrate */
		putreg32((uint32_t) isr_ch2infotable_ptr->brr, STM32_USART2_BRR);

		/* Already prepare the DMA stream */
		putreg32((uint32_t) isr_ch2infotable_ptr->buffer, mB_USART2TX_DMA_SxMOAR);				// Set the memory address
		putreg32((uint32_t) isr_ch2infotable_ptr->buf_length, mB_USART2TX_DMA_SxNDTR);			// Set the NDTR
		setbit32(mB_USART2TX_DMA_xIFCR, mB_USART2TX_DMA_INT_SxMASK);						// Clear pending flags for safety reasons

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
				}

		break;

	/* Tx start operation */
	case mB_CH2_TX_START:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx start specific operations now...
		 * Enable the previously prepared DMA stream now:
		 */
		setbit32(mB_USART2TX_DMA_SxCR, DMA_SCR_EN);

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Tx end operation */
	case mB_CH2_TX_END:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx end specific operations now...
		 * Reset the Data Enable pin first:
		 */
		setbit32(mB_CH2_DE_BSRR, mB_CH2_DE_OFF);

		/* Force the DMA stream to disable itself, which should already have happened after
		 * transmission of all bytes according to previous NDTR setting.
		 * Hence, this is only a safety feature to make sure that the next DMA transfer can
		 * start immediately and there is no pending data from the previous transfer. The
		 * disabling procedure may takes some time, as the DMA waits for the current transfer
		 * to complete before actually writing the DMA_SCR_EN bit to zero. But this is ok for
		 * our purposes, as the next transfer will not happen immediately.
		 */
		clearbit32(mB_USART2TX_DMA_SxCR, DMA_SCR_EN);

		/* Increment infotable pointer */
		isr_ch2infotable_ptr++;

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
					{
					isr_actiontable_ptr++;
					goto NEXT_ACTION;
					}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;
#endif // CONFIG_MESSBUS_USE_CH2

#if CONFIG_MESSBUS_USE_CH3
	/* Prepare Rx operation */
	case mB_CH3_RX_START:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Rx start specific operations now...
		 * Set baudrate first:
		 */
		putreg32((uint32_t) isr_ch3infotable_ptr->brr, STM32_USART3_BRR);

		/* Prepare and enable the DMA stream */
		putreg32((uint32_t) isr_ch3infotable_ptr->buffer, mB_USART3RX_DMA_SxMOAR);		// Set the memory address
		putreg32((uint32_t) 0xffff, mB_USART3RX_DMA_SxNDTR);						// Set the NDTR to max 0xffff
		setbit32(mB_USART3RX_DMA_xIFCR, mB_USART3RX_DMA_INT_SxMASK);				// Clear pending flags for safety reasons
		setbit32(mB_USART3RX_DMA_SxCR, DMA_SCR_EN);									// Enable the stream

		/* Enable the Receive Enable bit at the USART */
		setbit32(STM32_USART3_CR1, USART_CR1_RE);

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Stop Rx operation */
	case mB_CH3_RX_END:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Rx end specific operations now...
		 * Disable the Receive Enable bit at the USART first. Incomplete transfers will be aborted,
		 * so after enabling the receiver again it will start searching for a start bit again.
		 */
		clearbit32(STM32_USART3_CR1, USART_CR1_RE);

		/* Disable the DMA stream */
		clearbit32(mB_USART3RX_DMA_SxCR, DMA_SCR_EN);

		/* Write the number of received bytes directly into the slotlist (as feedback) */
		*isr_ch3infotable_ptr->buf_length_ptr = 0xffff - ((uint16_t) getreg32(mB_USART3RX_DMA_SxNDTR));

		/* Increment infotable pointer as we have completed this RX slot now. */
		isr_ch3infotable_ptr++;

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Tx data enable operation */
	case mB_CH3_TX_DE:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx data enable specific operations now...
		 * Set the Data Enable pin:
		 */
		setbit32(mB_CH3_DE_BSRR, mB_CH3_DE_ON);

		/* Set baudrate */
		putreg32((uint32_t) isr_ch3infotable_ptr->brr, STM32_USART3_BRR);

		/* Already prepare the DMA stream */
		putreg32((uint32_t) isr_ch3infotable_ptr->buffer, mB_USART3TX_DMA_SxMOAR);				// Set the memory address
		putreg32((uint32_t) isr_ch3infotable_ptr->buf_length, mB_USART3TX_DMA_SxNDTR);			// Set the NDTR
		setbit32(mB_USART3TX_DMA_xIFCR, mB_USART3TX_DMA_INT_SxMASK);						// Clear pending flags for safety reasons

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
				}

		break;

	/* Tx start operation */
	case mB_CH3_TX_START:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx start specific operations now...
		 * Enable the previously prepared DMA stream now:
		 */
		setbit32(mB_USART3TX_DMA_SxCR, DMA_SCR_EN);

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Tx end operation */
	case mB_CH3_TX_END:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx end specific operations now...
		 * Reset the Data Enable pin first:
		 */
		setbit32(mB_CH3_DE_BSRR, mB_CH3_DE_OFF);

		/* Force the DMA stream to disable itself, which should already have happened after
		 * transmission of all bytes according to previous NDTR setting.
		 * Hence, this is only a safety feature to make sure that the next DMA transfer can
		 * start immediately and there is no pending data from the previous transfer. The
		 * disabling procedure may takes some time, as the DMA waits for the current transfer
		 * to complete before actually writing the DMA_SCR_EN bit to zero. But this is ok for
		 * our purposes, as the next transfer will not happen immediately.
		 */
		clearbit32(mB_USART3TX_DMA_SxCR, DMA_SCR_EN);

		/* Increment infotable pointer */
		isr_ch3infotable_ptr++;

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
					{
					isr_actiontable_ptr++;
					goto NEXT_ACTION;
					}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;
#endif // CONFIG_MESSBUS_USE_CH3

#if CONFIG_MESSBUS_USE_CH4
	/* Prepare Rx operation */
	case mB_CH4_RX_START:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Rx start specific operations now...
		 * Set baudrate first:
		 */
		putreg32((uint32_t) isr_ch4infotable_ptr->brr, STM32_UART4_BRR);

		/* Prepare and enable the DMA stream */
		putreg32((uint32_t) isr_ch4infotable_ptr->buffer, mB_USART4RX_DMA_SxMOAR);		// Set the memory address
		putreg32((uint32_t) 0xffff, mB_USART4RX_DMA_SxNDTR);						// Set the NDTR to max 0xffff
		setbit32(mB_USART4RX_DMA_xIFCR, mB_USART4RX_DMA_INT_SxMASK);				// Clear pending flags for safety reasons
		setbit32(mB_USART4RX_DMA_SxCR, DMA_SCR_EN);									// Enable the stream

		/* Enable the Receive Enable bit at the USART */
		setbit32(STM32_UART4_CR1, USART_CR1_RE);

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Stop Rx operation */
	case mB_CH4_RX_END:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Rx end specific operations now...
		 * Disable the Receive Enable bit at the USART first. Incomplete transfers will be aborted,
		 * so after enabling the receiver again it will start searching for a start bit again.
		 */
		clearbit32(STM32_UART4_CR1, USART_CR1_RE);

		/* Disable the DMA stream */
		clearbit32(mB_USART4RX_DMA_SxCR, DMA_SCR_EN);

		/* Write the number of received bytes directly into the slotlist (as feedback) */
		*isr_ch4infotable_ptr->buf_length_ptr = 0xffff - ((uint16_t) getreg32(mB_USART4RX_DMA_SxNDTR));

		/* Increment infotable pointer as we have completed this RX slot now. */
		isr_ch4infotable_ptr++;

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Tx data enable operation */
	case mB_CH4_TX_DE:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx data enable specific operations now...
		 * Set the Data Enable pin:
		 */
		setbit32(mB_CH4_DE_BSRR, mB_CH4_DE_ON);

		/* Set baudrate */
		putreg32((uint32_t) isr_ch4infotable_ptr->brr, STM32_UART4_BRR);

		/* Already prepare the DMA stream */
		putreg32((uint32_t) isr_ch4infotable_ptr->buffer, mB_USART4TX_DMA_SxMOAR);				// Set the memory address
		putreg32((uint32_t) isr_ch4infotable_ptr->buf_length, mB_USART4TX_DMA_SxNDTR);			// Set the NDTR
		setbit32(mB_USART4TX_DMA_xIFCR, mB_USART4TX_DMA_INT_SxMASK);						// Clear pending flags for safety reasons

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
				}

		break;

	/* Tx start operation */
	case mB_CH4_TX_START:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx start specific operations now...
		 * Enable the previously prepared DMA stream now:
		 */
		setbit32(mB_USART4TX_DMA_SxCR, DMA_SCR_EN);

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Tx end operation */
	case mB_CH4_TX_END:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx end specific operations now...
		 * Reset the Data Enable pin first:
		 */
		setbit32(mB_CH4_DE_BSRR, mB_CH4_DE_OFF);

		/* Force the DMA stream to disable itself, which should already have happened after
		 * transmission of all bytes according to previous NDTR setting.
		 * Hence, this is only a safety feature to make sure that the next DMA transfer can
		 * start immediately and there is no pending data from the previous transfer. The
		 * disabling procedure may takes some time, as the DMA waits for the current transfer
		 * to complete before actually writing the DMA_SCR_EN bit to zero. But this is ok for
		 * our purposes, as the next transfer will not happen immediately.
		 */
		clearbit32(mB_USART4TX_DMA_SxCR, DMA_SCR_EN);

		/* Increment infotable pointer */
		isr_ch4infotable_ptr++;

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next action as ISR or wait for it if it's already too late. If the next
		 * action is scheduled as separate ISR make sure that it should not have triggered
		 * yet. Otherwise stay in the handler and go to next action manually. The pending
		 * interrupt will be acknowledged automatically then.
		 *
		 * This whole routine also works if the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + mB_EXTRA_WAIT_TICKS))
		{
			/* Schedule next interrupt */
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (isr_actiontable_ptr->ccr3 <= getreg16(STM32_TIM3_CNT))
					{
					isr_actiontable_ptr++;
					goto NEXT_ACTION;
					}
			isr_actiontable_ptr++;
		}
		else
		{
			/* Busy wait for next action */
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;
#endif // CONFIG_MESSBUS_USE_CH4

	}

#if mB_ISR_DEBUG_GPIO
	/* Reset Debug-GPIO */
	setbit32(STM32_GPIOD_BSRR, GPIO_DEBUG_ISR_OFF);
#endif
}

/****************************************************************************
 * Name: up_setup
 *
 * Description:
 *   Setup all required hardware and finally enable Timer 2 to trigger the
 *   first timeslice (never mind if a valid actionlist with N_ACTIONS >= 0
 *   already exists, if not don't perform anything in the ISRs and simply
 *   wait for next actionlist).
 *
 *   If required peripherals are selected in menuconfig then port and
 *   peripheral clocks are already enabled .
 *
 *   This method is called from public function messBUSMaster_initialize.c
 *   before registering the driver to the file system and thus making it
 *   applicable from application level.
 *
 ****************************************************************************/

static int up_setup(void)
{
	/* Setup Timer 2 in Mastermode and Timers 1 and 3 in triggered slave mode.
	 *
	 * Timer 1 outputs the Sync pulse, when triggered by Timer 2.
	 * Timer 3 generates interrupts
	 *
	 * Add additional Output Compare on PA15 -> PPS for testing purposes ???
	 *
	 */

	int ret;

#if mB_ISR_DEBUG_GPIO
	/* Configure GPIO in order to visualize the interrupts. */
	ret = stm32_configgpio(GPIO_DEBUG_ISR);
#endif

	/* Configure data enable (DE) pins */
#if CONFIG_MESSBUS_USE_CH1
	ret = stm32_configgpio(GPIO_CH1_DE);
#endif
#if CONFIG_MESSBUS_USE_CH2
	ret = stm32_configgpio(GPIO_CH2_DE);
#endif
#if CONFIG_MESSBUS_USE_CH3
	ret = stm32_configgpio(GPIO_CH3_DE);
#endif
#if CONFIG_MESSBUS_USE_CH4
	ret = stm32_configgpio(GPIO_CH4_DE);
#endif

	/* Configure receive enable (RE) pins.
	 * They will always output a low state.
	 */
#if CONFIG_MESSBUS_USE_CH1
	ret = stm32_configgpio(GPIO_CH1_RE);
#endif
#if CONFIG_MESSBUS_USE_CH2
	ret = stm32_configgpio(GPIO_CH2_RE);
#endif
#if CONFIG_MESSBUS_USE_CH3
	ret = stm32_configgpio(GPIO_CH3_RE);
#endif
#if CONFIG_MESSBUS_USE_CH4
	ret = stm32_configgpio(GPIO_CH4_RE);
#endif

	/* Attach TIM3 ram vector */
	ret = up_ramvec_attach(STM32_IRQ_TIM3, tim3_handler);
	if (ret < 0)
		{
			// Error handling???
		}

	/* Set the priority of the TIM3 interrupt vector */
	ret = up_prioritize_irq(STM32_IRQ_TIM3, NVIC_SYSH_PRIORITY_MAX); // NVIC_SYSH_HIGH_PRIORITY);
	if (ret < 0)
		{
			// Error handling???
		}

	/* Enable the timer interrupt at the NVIC */
	up_enable_irq(STM32_IRQ_TIM3);

	/* Make sure required peripheral clocks are enabled */
	modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM1EN);
	modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM2EN);
	modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM3EN);
	modifyreg32(STM32_RCC_AHB1ENR, 0, RCC_AHB1ENR_DMA2EN);
	modifyreg32(STM32_RCC_AHB1ENR, 0, RCC_AHB1ENR_DMA2EN);
#if CONFIG_MESSBUS_USE_CH1
	modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_USART1EN);
#endif
#if CONFIG_MESSBUS_USE_CH2
	modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_USART2EN);
#endif
#if CONFIG_MESSBUS_USE_CH3
	modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_USART3EN);
#endif
#if CONFIG_MESSBUS_USE_CH4
	modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_UART4EN);
#endif

	/* Configure timer related GPIOs as alternate functions */
#if CONFIG_MESSBUS_USE_CH1
	ret = stm32_configgpio(GPIO_TIM1_CH1OUT); 												// OC on PA8 or PE9
	clearbit32(mB_TIM1_CH1OUT_MODER, mB_TIM1_CH1OUT_HI_Z);
#endif
#if CONFIG_MESSBUS_USE_CH2
	ret = stm32_configgpio(GPIO_TIM1_CH2OUT); 												// OC on PA9 or PE11
	clearbit32(mB_TIM1_CH2OUT_MODER, mB_TIM1_CH2OUT_HI_Z);
#endif
#if CONFIG_MESSBUS_USE_CH3
	ret = stm32_configgpio(GPIO_TIM1_CH3OUT); 												// OC on PA10 or PE13
	clearbit32(mB_TIM1_CH3OUT_MODER, mB_TIM1_CH3OUT_HI_Z);
#endif
#if CONFIG_MESSBUS_USE_CH4
	ret = stm32_configgpio(GPIO_TIM1_CH4OUT); 												// OC on PA11 or PE14
	clearbit32(mB_TIM1_CH4OUT_MODER, mB_TIM1_CH4OUT_HI_Z);
#endif
#if mB_TIM2_OC_PA15 // Debug helper in order to visualize the trigger on an oscilloscope
	ret = stm32_configgpio(GPIO_TIM2_CH1OUT_2);												// OC on PA15
#endif


	/* Configure Timer 1 as slave of Timer 2 in triggered one pulse mode */
	putreg16(mB_TIM1_PSC, STM32_TIM1_PSC);													// Configure prescaler
	modifyreg16(STM32_TIM1_EGR, 0, ATIM_EGR_UG);											// Force update event to enable prescaler setting
	putreg16(mB_TIM1_PERIOD, STM32_TIM1_ARR);												// Configure period
	modifyreg16(STM32_TIM1_CR1, 0, ATIM_CR1_OPM);											// One pulse mode
	modifyreg16(STM32_TIM1_SMCR, 0, ATIM_SMCR_ITR1);										// Select ITR1 as input trigger
	modifyreg16(STM32_TIM1_SMCR, 0, ATIM_SMCR_TRIGGER);										// Select slave trigger mode
	modifyreg16(STM32_TIM1_EGR, 0, ATIM_EGR_UG);											// Enable update event
	modifyreg16(STM32_TIM1_BDTR, 0, ATIM_BDTR_MOE);											// Main output enable
#if CONFIG_MESSBUS_USE_CH1
	/* Channel 1 */
	putreg16(mB_TIM1_PULSE, STM32_TIM1_CCR1);												// Configure pulse width (SYNC)
	modifyreg16(STM32_TIM1_CCMR1, 0, (ATIM_CCMR_MODE_PWM2 << ATIM_CCMR1_OC1M_SHIFT));		// PWM2-Mode
	modifyreg16(STM32_TIM1_CCMR1, 0, ATIM_CCMR1_OC1PE);										// Output compare 1 preload enable
	modifyreg16(STM32_TIM1_CCER, 0, ATIM_CCER_CC1P);										// Output polarity active low
	modifyreg16(STM32_TIM1_CCER, 0, ATIM_CCER_CC1E);										// Output enable
#endif
#if CONFIG_MESSBUS_USE_CH2
	/* Channel 2 */
	putreg16(mB_TIM1_PULSE, STM32_TIM1_CCR2);												// Configure pulse width (SYNC)
	modifyreg16(STM32_TIM1_CCMR1, 0, (ATIM_CCMR_MODE_PWM2 << ATIM_CCMR1_OC2M_SHIFT));		// PWM2-Mode
	modifyreg16(STM32_TIM1_CCMR1, 0, ATIM_CCMR1_OC2PE);										// Output compare 1 preload enable
	modifyreg16(STM32_TIM1_CCER, 0, ATIM_CCER_CC2P);										// Output polarity active low
	modifyreg16(STM32_TIM1_CCER, 0, ATIM_CCER_CC2E);										// Output enable
#endif
#if CONFIG_MESSBUS_USE_CH3
	/* Channel 3 */
	putreg16(mB_TIM1_PULSE, STM32_TIM1_CCR3);												// Configure pulse width (SYNC)
	modifyreg16(STM32_TIM1_CCMR2, 0, (ATIM_CCMR_MODE_PWM2 << ATIM_CCMR2_OC3M_SHIFT));		// PWM2-Mode
	modifyreg16(STM32_TIM1_CCMR2, 0, ATIM_CCMR2_OC3PE);										// Output compare 1 preload enable
	modifyreg16(STM32_TIM1_CCER, 0, ATIM_CCER_CC3P);										// Output polarity active low
	modifyreg16(STM32_TIM1_CCER, 0, ATIM_CCER_CC3E);										// Output enable
#endif
#if CONFIG_MESSBUS_USE_CH4
	/* Channel 4 */
	putreg16(mB_TIM1_PULSE, STM32_TIM1_CCR4);												// Configure pulse width (SYNC)
	modifyreg16(STM32_TIM1_CCMR2, 0, (ATIM_CCMR_MODE_PWM2 << ATIM_CCMR2_OC4M_SHIFT));		// PWM2-Mode
	modifyreg16(STM32_TIM1_CCMR2, 0, ATIM_CCMR2_OC4PE);										// Output compare 1 preload enable
	modifyreg16(STM32_TIM1_CCER, 0, ATIM_CCER_CC4P);										// Output polarity active low
	modifyreg16(STM32_TIM1_CCER, 0, ATIM_CCER_CC4E);										// Output enable
#endif
	/* Don't enable timer 1 already. It is triggered by timer 2. */

	/* Configure Timer 3 as slave of Timer 2 in triggered interrupt mode. */
	putreg16(mB_TIM3_PSC, STM32_TIM3_PSC);													// Configure prescaler
	modifyreg16(STM32_TIM3_SMCR, 0, GTIM_SMCR_ITR1);										// Select ITR1 as input trigger
	modifyreg16(STM32_TIM3_SMCR, 0, GTIM_SMCR_TRIGGER);										// Select slave trigger mode
	modifyreg16(STM32_TIM3_EGR, 0, GTIM_EGR_UG);											// Force update event to enable prescaler setting
	modifyreg16(STM32_TIM3_SR, GTIM_SR_UIF, 0);												// Acknowledge the previous update event
	modifyreg16(STM32_TIM3_DIER, 0, GTIM_DIER_TIE);											// Enable trigger interrupt
	putreg16(0xffff, STM32_TIM3_CCR3);														// Set compare value to maximum to avoid early compare interrupt
	modifyreg16(STM32_TIM3_DIER, 0, GTIM_DIER_CC3IE);										// Enable compare interrupts for channel 3
	/* Don't enable timer 3 already. It is triggered by timer 2. */

	/* Configure USART related GPIOs as alternate functions. */
#if CONFIG_MESSBUS_USE_CH1
	ret = stm32_configgpio(GPIO_USART1_RX);				// RX and IC on PC7
	ret = stm32_configgpio(GPIO_USART1_TX);				// USART6 on PC6 (probably)
	clearbit32(mB_USART1TX_MODER, mB_USART1TX_HI_Z);
#endif
#if CONFIG_MESSBUS_USE_CH2
	ret = stm32_configgpio(GPIO_USART2_RX);
	ret = stm32_configgpio(GPIO_USART2_TX);
	clearbit32(mB_USART2TX_MODER, mB_USART2TX_HI_Z);
#endif
#if CONFIG_MESSBUS_USE_CH3
	ret = stm32_configgpio(GPIO_USART3_RX);
	ret = stm32_configgpio(GPIO_USART3_TX);
	clearbit32(mB_USART3TX_MODER, mB_USART3TX_HI_Z);
#endif
#if CONFIG_MESSBUS_USE_CH4
	ret = stm32_configgpio(GPIO_UART4_RX);
	ret = stm32_configgpio(GPIO_UART4_TX);
	clearbit32(mB_USART4TX_MODER, mB_USART4TX_HI_Z);
#endif

	uint32_t scr;
#if CONFIG_MESSBUS_USE_CH1
	/* Try to take USART1's Rx and Tx DMA streams in the OS. They will be locked with
	 * a semaphore so that no other driver can use the streams. If another
	 * driver already uses the stream stm32_dmachannel waits until the
	 * specified stream is released. This is a safety feature to make sure
	 * the driver has exclusive access to the DMA stream.
	 *
	 * Also initially configure the streams with the method stm32_dmasetup.
	 */
	DMA_HANDLE usart1rx_dma;
	scr = (DMA_SCR_DIR_P2M | DMA_SCR_MINC | DMA_SCR_PRIVERYHI);													// Configure SCR register, Priority???
	usart1rx_dma = stm32_dmachannel(DMAMAP_USART1_RX);
#if defined(CONFIG_ARCH_CHIP_STM32F7)
	stm32_dmasetup(usart1rx_dma, STM32_USART1_RDR, 0, 0, scr);
#elif defined(CONFIG_STM32_STM32F4XXX)
	stm32_dmasetup(usart1rx_dma, STM32_USART1_DR, 0, 0, scr);
#endif

	DMA_HANDLE usart1tx_dma;
	scr = (DMA_SCR_DIR_M2P | DMA_SCR_MINC | DMA_SCR_PRIHI);													// Configure SCR register, Priority???
	usart1tx_dma = stm32_dmachannel(DMAMAP_USART1_TX);
#if defined(CONFIG_ARCH_CHIP_STM32F7)
	stm32_dmasetup(usart1tx_dma, STM32_USART1_TDR, 0, 0, scr);
#elif defined(CONFIG_STM32_STM32F4XXX)
	stm32_dmasetup(usart1tx_dma, STM32_USART1_DR, 0, 0, scr);
#endif
#endif //CONFIG_MESSBUS_USE_CH1

#if CONFIG_MESSBUS_USE_CH2
	/* Try to take USART2's Rx and Tx DMA streams in the OS. They will be locked with
	 * a semaphore so that no other driver can use the streams. If another
	 * driver already uses the stream stm32_dmachannel waits until the
	 * specified stream is released. This is a safety feature to make sure
	 * the driver has exclusive access to the DMA stream.
	 *
	 * Also initially configure the streams with the method stm32_dmasetup.
	 */
	DMA_HANDLE usart2rx_dma;
	scr = (DMA_SCR_DIR_P2M | DMA_SCR_MINC | DMA_SCR_PRIVERYHI);													// Configure SCR register, Priority???
	usart2rx_dma = stm32_dmachannel(DMAMAP_USART2_RX);
#if defined(CONFIG_ARCH_CHIP_STM32F7)
	stm32_dmasetup(usart2rx_dma, STM32_USART2_RDR, 0, 0, scr);
#elif defined(CONFIG_STM32_STM32F4XXX)
	stm32_dmasetup(usart2rx_dma, STM32_USART2_DR, 0, 0, scr);
#endif

	DMA_HANDLE usart2tx_dma;
	scr = (DMA_SCR_DIR_M2P | DMA_SCR_MINC | DMA_SCR_PRIHI);													// Configure SCR register, Priority???
	usart2tx_dma = stm32_dmachannel(DMAMAP_USART2_TX);
#if defined(CONFIG_ARCH_CHIP_STM32F7)
	stm32_dmasetup(usart2tx_dma, STM32_USART2_TDR, 0, 0, scr);
#elif defined(CONFIG_STM32_STM32F4XXX)
	stm32_dmasetup(usart2tx_dma, STM32_USART2_DR, 0, 0, scr);
#endif
#endif //CONFIG_MESSBUS_USE_CH2

#if CONFIG_MESSBUS_USE_CH3
	/* Try to take USART3's Rx and Tx DMA streams in the OS. They will be locked with
	 * a semaphore so that no other driver can use the streams. If another
	 * driver already uses the stream stm32_dmachannel waits until the
	 * specified stream is released. This is a safety feature to make sure
	 * the driver has exclusive access to the DMA stream.
	 *
	 * Also initially configure the streams with the method stm32_dmasetup.
	 */
	DMA_HANDLE usart3rx_dma;
	scr = (DMA_SCR_DIR_P2M | DMA_SCR_MINC | DMA_SCR_PRIVERYHI);													// Configure SCR register, Priority???
	usart3rx_dma = stm32_dmachannel(DMAMAP_USART3_RX);
#if defined(CONFIG_ARCH_CHIP_STM32F7)
	stm32_dmasetup(usart3rx_dma, STM32_USART3_RDR, 0, 0, scr);
#elif defined(CONFIG_STM32_STM32F4XXX)
	stm32_dmasetup(usart3rx_dma, STM32_USART3_DR, 0, 0, scr);
#endif

	DMA_HANDLE usart3tx_dma;
	scr = (DMA_SCR_DIR_M2P | DMA_SCR_MINC | DMA_SCR_PRIHI);													// Configure SCR register, Priority???
	usart3tx_dma = stm32_dmachannel(DMAMAP_USART3_TX);
#if defined(CONFIG_ARCH_CHIP_STM32F7)
	stm32_dmasetup(usart3tx_dma, STM32_USART3_TDR, 0, 0, scr);
#elif defined(CONFIG_STM32_STM32F4XXX)
	stm32_dmasetup(usart3tx_dma, STM32_USART3_DR, 0, 0, scr);
#endif
#endif //CONFIG_MESSBUS_USE_CH3

#if CONFIG_MESSBUS_USE_CH4
	/* Try to take USART4's Rx and Tx DMA streams in the OS. They will be locked with
	 * a semaphore so that no other driver can use the streams. If another
	 * driver already uses the stream stm32_dmachannel waits until the
	 * specified stream is released. This is a safety feature to make sure
	 * the driver has exclusive access to the DMA stream.
	 *
	 * Also initially configure the streams with the method stm32_dmasetup.
	 */
	DMA_HANDLE usart4rx_dma;
	scr = (DMA_SCR_DIR_P2M | DMA_SCR_MINC | DMA_SCR_PRIVERYHI);													// Configure SCR register, Priority???
	usart4rx_dma = stm32_dmachannel(DMAMAP_UART4_RX);
#if defined(CONFIG_ARCH_CHIP_STM32F7)
	stm32_dmasetup(usart4rx_dma, STM32_UART4_RDR, 0, 0, scr);
#elif defined(CONFIG_STM32_STM32F4XXX)
	stm32_dmasetup(usart4rx_dma, STM32_UART4_DR, 0, 0, scr);
#endif

	DMA_HANDLE usart4tx_dma;
	scr = (DMA_SCR_DIR_M2P | DMA_SCR_MINC | DMA_SCR_PRIHI);													// Configure SCR register, Priority???
	usart4tx_dma = stm32_dmachannel(DMAMAP_UART4_TX);
#if defined(CONFIG_ARCH_CHIP_STM32F7)
	stm32_dmasetup(usart4tx_dma, STM32_UART4_TDR, 0, 0, scr);
#elif defined(CONFIG_STM32_STM32F4XXX)
	stm32_dmasetup(usart4tx_dma, STM32_UART4_DR, 0, 0, scr);
#endif
#endif //CONFIG_MESSBUS_USE_CH4

	/* Setup the USARTs.
	 * Don't configure baudrates already. This will be done via
	 * convert_slotlist and the ISR.
	 */
#if CONFIG_MESSBUS_USE_CH1
	modifyreg32(STM32_USART1_CR3, 0, USART_CR3_DMAR);			// Enable DMA receiver for USART1
	modifyreg32(STM32_USART1_CR3, 0, USART_CR3_DMAT);			// Enable DMA transmitter for USART1
	modifyreg32(STM32_USART1_CR1, 0, USART_CR1_OVER8);			// Enable oversampling by 8
#endif
#if CONFIG_MESSBUS_USE_CH2
	modifyreg32(STM32_USART2_CR3, 0, USART_CR3_DMAR);			// Enable DMA receiver for USART2
	modifyreg32(STM32_USART2_CR3, 0, USART_CR3_DMAT);			// Enable DMA transmitter for USART2
	modifyreg32(STM32_USART2_CR1, 0, USART_CR1_OVER8);			// Enable oversampling by 8
#endif
#if CONFIG_MESSBUS_USE_CH3
	modifyreg32(STM32_USART3_CR3, 0, USART_CR3_DMAR);			// Enable DMA receiver for USART3
	modifyreg32(STM32_USART3_CR3, 0, USART_CR3_DMAT);			// Enable DMA transmitter for USART3
	modifyreg32(STM32_USART3_CR1, 0, USART_CR1_OVER8);			// Enable oversampling by 8
#endif
#if CONFIG_MESSBUS_USE_CH4
	modifyreg32(STM32_UART4_CR3, 0, USART_CR3_DMAR);			// Enable DMA receiver for USART4
	modifyreg32(STM32_UART4_CR3, 0, USART_CR3_DMAT);			// Enable DMA transmitter for USART4
	modifyreg32(STM32_UART4_CR1, 0, USART_CR1_OVER8);			// Enable oversampling by 8
#endif

	return 0;
}

static int up_start(void)
{

	/* Start the USARTs */
#if CONFIG_MESSBUS_USE_CH1
	modifyreg32(STM32_USART1_CR1, 0, USART_CR1_UE);				// Enable USART1
	modifyreg32(STM32_USART1_CR1, 0, USART_CR1_TE);				// Enable the transmitter
#endif
#if CONFIG_MESSBUS_USE_CH2
	modifyreg32(STM32_USART2_CR1, 0, USART_CR1_UE);				// Enable USART2
	modifyreg32(STM32_USART2_CR1, 0, USART_CR1_TE);				// Enable the transmitter
#endif
#if CONFIG_MESSBUS_USE_CH3
	modifyreg32(STM32_USART3_CR1, 0, USART_CR1_UE);				// Enable USART3
	modifyreg32(STM32_USART3_CR1, 0, USART_CR1_TE);				// Enable the transmitter
#endif
#if CONFIG_MESSBUS_USE_CH4
	modifyreg32(STM32_UART4_CR1, 0, USART_CR1_UE);				// Enable USART4
	modifyreg32(STM32_UART4_CR1, 0, USART_CR1_TE);				// Enable the transmitter
#endif

	/* Configure Timer 2 as master in output compare mode */
	putreg16(mB_TIM2_PSC, STM32_TIM2_PSC);													// Configure prescaler
	modifyreg16(STM32_TIM2_EGR, 0, GTIM_EGR_UG);											// Force update event to enable prescaler setting
	putreg32(mB_TIM2_PERIOD, STM32_TIM2_ARR);												// Configure period
	/* Debug helper in order to visualize the trigger on an oscilloscope */
#if mB_TIM2_OC_PA15
	putreg32(mB_TIM2_PULSE, STM32_TIM2_CCR1);												// Configure pulse width (SYNC)
	modifyreg16(STM32_TIM2_CCMR1, 0, (GTIM_CCMR_MODE_PWM1 << GTIM_CCMR1_OC1M_SHIFT));		// PWM1-Mode
	modifyreg16(STM32_TIM2_CCMR1, 0, ATIM_CCMR1_OC1PE);										// Output compare 1 preload enable
	modifyreg16(STM32_TIM2_CCER, 0, GTIM_CCER_CC1E);										// Output enable
#endif // mB_TIM2_OC_PA15, debug helper
	modifyreg16(STM32_TIM2_CR2, 0, GTIM_CR2_MMS_UPDATE);									// Trigger update mode
	modifyreg16(STM32_TIM2_CR1, 0, GTIM_CR1_CEN);											// Enable the timer

	return 0;
}

/****************************************************************************
 * Name: up_stop
 *
 * Description:
 *   Not provided at this stage of development. Should principally be the
 *   counterpart to up_start.
 *
 ****************************************************************************/

static int up_stop(void)
{
	/* Maybe needed later, so the function is already provided and can be
	 * called from the upper half as it is bound to it via messBUS_ops_s.
	 */

	return 0;
}

/****************************************************************************
 * Name: up_shutdown
 *
 * Description:
 *   Not provided at this stage of development. Should principally be the
 *   counterpart to up_setup.
 *
 ****************************************************************************/

static int up_shutdown(void)
{
	/* Maybe needed later, so the function is already provided and can be
	 * called from the upper half as it is bound to it via messBUS_ops_s.
	 */

	return 0;
}

/****************************************************************************
 * Name: up_update_buffers
 *
 * Description:
 *   Invalidates the F7 chip's data cache to make sure the CPU uses the
 *   newest rx buffers. As only the F7 architecture has a cache, this
 *   function is only effective for F7 chips.
 *
 *   This is a rather bad workaround, because the call consumes about 10us.
 *   A better approach would be to use the MPU to declare the buffers
 *   uncacheable within the application.
 *
 *   Another problem is that the hardware abstraction of the upper half is
 *   partly violated, because it has to know whether a F7 chip is used or not.
 *   This can only be overcome by having a buffer inside the driver itself
 *   parsing the data to the destination buffers via a memcpy and not via DMA.
 *   This approach would on the one hand certainly decrease efficiency and
 *   increase latencies on the other hand.
 *
 ****************************************************************************/

static void up_update_buffers(void)
{
#ifdef CONFIG_ARCH_CHIP_STM32F7
	/* Invalidate the whole cache. This method takes approx. 10 us which
	 * is quite a long time. This could be improved by using single cache
	 * lines or the MPU.
	 */
	arch_invalidate_dcache_all();
#endif
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

void messBUSMaster_initialize(void)
{
	/* Perform registration of the driver.
	 * This method should be called from board-specific stm32_bringup.c
	 */
	int ret;

	/* Call the upper half to register the whole driver in the VFS. */
	ret = messBUSMaster_register("/dev/messBusMaster", &g_messBUSpriv);
	if (ret < 0)
	{
		// Error handling??
	}
}


