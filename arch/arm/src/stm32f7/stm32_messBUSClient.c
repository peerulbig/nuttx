/****************************************************************************
 * arch/arm/src/stm32f7/stm32_messBUSClient.c
 *
 *   Author: Peer Ulbig <p.ulbig@tu-braunschweig.de>
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/arch.h>
#include <nuttx/irq.h>
#include <arch/board/board.h>
#include <arch/chip/chip.h>
#include <arch/irq.h>
#include <arch/chip/irq.h>
#include <debug.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <sched.h>
#include <stdio.h>						// printf

#include "up_arch.h" 					// putreg32, modifyreg16 etc.
#include "chip.h"
#include "stm32_gpio.h"
#include "chip/stm32_tim.h"
#include "chip/stm32_gpio.h"
#include "chip/stm32_uart.h"
#include "up_internal.h"
#include "ram_vectors.h"
#include "stm32_dma.h"
#include "stm32_rcc.h"
#include "stm32_uart.h"
#include "stm32_tim.h"
#ifdef CONFIG_ARCH_CHIP_STM32F7
	#include "cache.h"
#endif
#include "stm32_messBUSClient.h"

/* Header file which is also used in application and board specific bringup.c */
#include <nuttx/messBUS/messBUSClient.h>


/****************************************************************************
 * Non-atomic hardware access helper functions
 ****************************************************************************/

/****************************************************************************
 * Name: setbit32
 *
 * Description:
 *   Set a single bit in 32-bit register *target with read-modify-write operation.
 *   Suppress compiler optimizations using volatile and avoid extra clock
 *   cycles for function call by declaring the function always inline.
 *
 ****************************************************************************/

static inline void setbit32(unsigned int target, uint32_t bit) __attribute__((always_inline));
static inline void setbit32(unsigned int target, uint32_t bit){
	*(volatile uint32_t *)(target) |= (bit);
};

/****************************************************************************
 * Name: clearbit32
 *
 * Description:
 *   Clear a single bit in 32-bit register *target with read-modify-write operation.
 *   Suppress compiler optimizations using volatile and avoid extra clock
 *   cycles for function call by declaring the function always inline.
 *
 ****************************************************************************/

static inline void clearbit32(unsigned int target, uint32_t bit) __attribute__((always_inline));
static inline void clearbit32(unsigned int target, uint32_t bit){
	*(volatile uint32_t *)(target) &= ~(bit);
};

/****************************************************************************
 * Name: setbit16
 *
 * Description:
 *   Set a single bit in 16-bit register *target with read-modify-write operation.
 *   Suppress compiler optimizations using volatile and avoid extra clock
 *   cycles for function call by declaring the function always inline.
 *
 ****************************************************************************/

static inline void setbit16(unsigned int target, uint16_t bit) __attribute__((always_inline));
static inline void setbit16(unsigned int target, uint16_t bit){
	*(volatile uint16_t *)(target) |= (bit);
};

/****************************************************************************
 * Name: clearbit16
 *
 * Description:
 *   Clear a single bit in 16-bit register *target with read-modify-write operation.
 *   Suppress compiler optimizations using volatile and avoid extra clock
 *   cycles for function call by declaring the function always inline.
 *
 ****************************************************************************/

static inline void clearbit16(unsigned int target, uint16_t bit) __attribute__((always_inline));
static inline void clearbit16(unsigned int target, uint16_t bit){
	*(volatile uint16_t *)(target) &= ~(bit);
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static uint16_t up_baud_to_brr (uint32_t baud);
static void up_convert_slotlist(void *slotlist, void *actiontable, void *infotable);
static void tim3_handler(void);
static int up_setup(void);
static int up_shutdown(void);
static int up_start(void);
static int up_stop(void);
static void up_update_buffers(void);

/****************************************************************************
 * Private Types
 ****************************************************************************/

/* This struct contains the timing and op_mode information for a single
 * action in interrupt context.
 */
struct up_isr_action_s
{
	uint16_t 	ccr3;			// Next counter value to program into Timer 3 CCR3 register
	uint8_t		op_mode;		// Next ISR operation mode / case
};

/* This struct contains the data related information of a single slot.
 * This struct can be used in more than one action and is thus seperated
 * from the action structs.
 */
struct up_isr_info_s
{
	uint16_t	brr;
	char		*buffer;
	uint16_t	buf_length;
	uint16_t	*buf_length_ptr;
};

/****************************************************************************
 * Private Data
 ****************************************************************************/

/* Arch-specific instance of messBUS_ops_s, which will be attached to
 * g_messBUSpriv and thus to the upper half driver.
 */
static const struct messBUS_ops_s g_messBUS_ops =
{
  .setup			= up_setup,
  .shutdown			= up_shutdown,
  .start			= up_start,
  .stop				= up_stop,
  .convert_slotlist	= up_convert_slotlist,
  .update_buffers	= up_update_buffers
};

/* Default operation mode for next action */
static volatile uint8_t g_op_mode = 0;

/* Consecutive Sync errors */
static volatile uint8_t g_sync_err = 0;

/* Consecutive phase errors */
static volatile uint8_t g_phase_err = 0;

/* Timer 3 CCR1 and CCR2 values (to synchronize with SYNC pulse) */
static volatile uint16_t g_tim3_ccr1 = 0;
static volatile uint16_t g_tim3_ccr2 = 0;

/* Update event ticks. This will be the Timer 3's ARR value, which can
 * also contain clock correction ticks if board.h's mB_TICKS_PER_TIMESLICE
 * is not equal to 40000.
 */
static volatile uint16_t g_ue_ticks = 0;

/* Action tables and respective pointer. We always need a further action for
 * SYNC preparation (case 2) at the end of each timeslice.
 */
static struct up_isr_action_s isr_actiontable0[((2 * CONFIG_MESSBUS_CH1_MAX_RX_SLOTS) + (3 * CONFIG_MESSBUS_CH1_MAX_TX_SLOTS) + 1)];
static struct up_isr_action_s isr_actiontable1[((2 * CONFIG_MESSBUS_CH1_MAX_RX_SLOTS) + (3 * CONFIG_MESSBUS_CH1_MAX_TX_SLOTS) + 1)];
static struct up_isr_action_s *isr_actiontable_ptr = isr_actiontable0;

/* Isr info tables, one per each slot */
struct up_isr_info_s isr_infotable0[(CONFIG_MESSBUS_CH1_MAX_RX_SLOTS + CONFIG_MESSBUS_CH1_MAX_TX_SLOTS)] = {0};
struct up_isr_info_s isr_infotable1[(CONFIG_MESSBUS_CH1_MAX_RX_SLOTS + CONFIG_MESSBUS_CH1_MAX_TX_SLOTS)] = {0};
static struct up_isr_info_s *isr_infotable_ptr = isr_infotable0;

/* Arch-specific instance of messBUS_dev_t, which will be attached to the
 * upper half driver. This struct is used to bind the lower half to its
 * upper half and to share data between them.
 */
static messBUS_dev_t g_messBUSpriv =
{
	/* This variable indicates whether the device has already been opened
	 * or not. Default is closed (0).
	 */
	.open = 0,

	/* This variable indicates whether the driver is running or paused.
	 * A user always needs to open the device first and attach a slotlist
	 * via ioctl before he can actually start the driver. Default is
	 * paused (0).
	 */
	.running = 0,

	/* This variable is incremented by the interrupt handler after each
	 * successful SYNC. Higher logic can poll for this variable to become
	 * non-zero when it waits for the next timeslice. If the value of this
	 * variable is > 1 higher logic can recognise that it missed x time-
	 * slices.
	 */
	.sync_count = 0,

	/* This variable indicates whether the interrupt handler currently
	 * points to tables with index 0 or 1. It is modified by the interrupt
	 * handler. The interrupt handler only changes tables when there are
	 * new tables available. The unused tables can be modified by higher
	 * logic.
	 */
	.active_table = 0,

	/* This variable indicates that new tables are available. It is set by
	 * higher logic and cleared by the interrupt handler after the current
	 * time slice. If set to one by higher logic the interrupt handler will
	 * point to the modified tables after the current time slice.
	 */
	.new_table_avail = 0,

	/* These values are written or read respectively when it comes to calls
	 * to the ioctl commands MESSBUSIOC_GET_PHASE_DEVIATION or
	 * MESSBUSIOC_SET_PHASE_CORRECTION. They can be used to dynamically
	 * compensate clock drifts by application logic.
	 */
	.phase_dev = 0,
	.phase_cor = 0,

	/* Let the upper half know where to find the tables for the interrupt
	 * routine. This information is needed when a new slotlist is attached
	 * to the upper half driver.
	 */
	.isr_actiontable0 = isr_actiontable0,
	.isr_actiontable1 = isr_actiontable1,
	.isr_infotable0 = isr_infotable0,
	.isr_infotable1 = isr_infotable1,

	/* Attach the lower half operations to the upper half */
	.ops = &g_messBUS_ops,
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: baud_to_brr
 *
 * Description:
 *   This function converts a baudrate into a BRR register value. The way the
 *   conversion has to be done differs for the F4 and F7 chips. See their
 *   reference manuals for more information.
 *
 * Assumptions:
 *   - Note that the F7 chips have to be clocked by 216MHz SYSCLK, which has
 *     to be configured in board.h respectively.
 *
 *   - This implementation requires that oversampling by 8 is configured.
 *     Oversampling by 16 is not supported, because messBUS' relatively high
 *     baudrates can be represented way more accurately when using over-
 *     sampling by 8 in the very most cases.
 *
 ****************************************************************************/
#if defined(CONFIG_ARCH_CHIP_STM32F7)
static uint16_t up_baud_to_brr (uint32_t baud)
{
	/* As we oversample by 8, the equation is:
	 *
	 *   baud      = 2 * fCK / usartdiv8
	 *   usartdiv8 = 2 * fCK / baud
	 *
	 * fCK is the 216 Mhz SYSCLK, which allows very high baudrates. Make
	 * sure SYSCLK is selected as clock source in board.h.
	 *
	 * As we oversample by 8, magical bit shifting operation is required for
	 * bits 0 to 3 in order to obtain brr register value from usartdiv8.
	 *
	 * Baud/2 is added to fCK to support rounding.
	 */
	uint16_t usartdiv8 = (uint16_t) (((STM32_SYSCLK_FREQUENCY << 1) + (baud >> 1)) / baud);
	uint16_t brr = ((usartdiv8 & 0xfff0) | ((usartdiv8 & 0x000f) >> 1));

	return brr;
}
#elif defined(CONFIG_STM32_STM32F4XXX)
static uint16_t up_baud_to_brr (uint32_t baud)
{
	/* This second implementation is for U[S]ARTs that support fractional
	 * dividers, such as the F4's USARTS.
	 *
	 * Configure the USART Baud Rate.  The baud rate for the receiver and
	 * transmitter (Rx and Tx) are both set to the same value as programmed
	 * in the Mantissa and Fraction values of USARTDIV. In case of
	 * oversampling by 8 the equations are:
	 *
	 *   baud     = fCK / (8 * usartdiv)
	 *   usartdiv = fCK / (8 * baud)
	 *
	 * Where fCK is the input clock to the peripheral (PCLK1 for USART2, 3, 4, 5
	 * or PCLK2 for USART1 and USART6)
	 *
	 * First calculate (NOTE: all stand baud values are even so dividing by two
	 * does not lose precision):
	 *
	 *   usartdiv32 = 32 * usartdiv = fCK / (baud/2)
	 */
	uint16_t usartdiv32 = (uint16_t) (STM32_PCLK2_FREQUENCY / (baud >> 1));

	/* The mantissa part is then */
	uint16_t mantissa = (usartdiv32 >> 4);

	/* The fractional remainder (with rounding) */
	uint16_t fraction = ((usartdiv32 - (mantissa << 4) + 1) >> 1);

	/* Place both values in the brr which will be returned */
	uint16_t brr = (mantissa << USART_BRR_MANT_SHIFT);
	brr |= (fraction << USART_BRR_FRAC_SHIFT);

	return brr;
}
#endif

/****************************************************************************
 * Name: up_convert_slotlist
 *
 * Description:
 *   This function converts a slotlist into both a action- and infotable,
 *   which the interrupt handler will use to perform the desired slots.
 *
 * Input arguments:
 *   slotlist - A reference to the slotlist
 *   actiontable - A reference to a preallocated actiontable
 *   infotable - A reference to a preallocated infotable
 *
 ****************************************************************************/

static void up_convert_slotlist(void *slotlist, void *actiontable, void *infotable)
{
	uint8_t remaining_slots = ((struct slotlist1_s *)slotlist)->n_slots;
	struct slot_s *slot_ptr = ((struct slotlist1_s *)slotlist)->slot;
	struct up_isr_action_s *action_ptr = actiontable;
	struct up_isr_info_s *info_ptr = infotable;
	uint16_t nom_ticks;
	int32_t real_ticks;
	int32_t dyn_denom;

	/* Calculate the dynamic denominator for phase corrections, which results
	 * from calls to MESSBUSIOC_SET_PHASE_CORRECTION ioctl command. Avoid
	 * division by zero.
	 */
	if(g_messBUSpriv.phase_cor != 0)
	{
		dyn_denom = 40000 / g_messBUSpriv.phase_cor;
	}
	else
	{
		/* Set it to 50000 so that it has no effect */
		dyn_denom = 50000;
	}


	while (remaining_slots > 0)
	{
		if (slot_ptr->read_write == MESSBUS_TX)
		{
			/* Tx operation has three actions:
			 *
			 * - CH1_TX_DE (Data Enable)
			 * - CH1_TX_START
			 * - CH1_TX_END
			 *
			 * So we create three timed actions and a single info structure for
			 * this slot. We start with the first action:
			 */
			action_ptr->op_mode = mB_CH1_TX_DE;
			nom_ticks = ((slot_ptr->t_start) * 4) - mB_OFFSET_TICKS;
			real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR) +
					(nom_ticks / dyn_denom);
			action_ptr->ccr3 = (uint16_t) real_ticks;
			action_ptr++;

			/* Schedule second action two ticks (0.5us) after first action. */
			action_ptr->op_mode = mB_CH1_TX_START;
			nom_ticks = ((slot_ptr->t_start) * 4) - mB_OFFSET_TICKS + 2;
			real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR) +
					(nom_ticks / dyn_denom);
			action_ptr->ccr3 = (uint16_t) real_ticks;
			action_ptr++;

			/* Third action: */
			action_ptr->op_mode = mB_CH1_TX_END;
			nom_ticks = ((slot_ptr->t_end) * 4) - mB_OFFSET_TICKS;
			real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR) +
					(nom_ticks / dyn_denom);
			action_ptr->ccr3 = (uint16_t) real_ticks;
			action_ptr++;
		}
		else
		{
			/* Tx operation has two actions:
			 *
			 * - CH1_RX_START
			 * - CH1_RX_END
			 *
			 * So we create two timed actions and a single info structure for
			 * this slot. We start with the first action:
			 */
			/*
			 * messBUS node need to begin offset us before rx slot with its preparation
			 * to ensure that it is ready to read data an slot begin
			 */
			uint16_t t_start_corr = slot_ptr->t_start + mB_RX_OFFSET_US;

			action_ptr->op_mode = mB_CH1_RX_START;
			nom_ticks = ((t_start_corr) * 4) - mB_OFFSET_TICKS;
			real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR) +
					(nom_ticks / dyn_denom);
			action_ptr->ccr3 = (uint16_t) real_ticks;
			action_ptr++;

			/* Second action: */
			action_ptr->op_mode = mB_CH1_RX_END;
			nom_ticks = ((slot_ptr->t_end) * 4) - mB_OFFSET_TICKS;
			real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR) +
					(nom_ticks / dyn_denom);
			action_ptr->ccr3 = (uint16_t) real_ticks;
			action_ptr++;
		}

		/* Finally the info for this slot, which is the same regardless of
		 * TX or RX operation:
		 */
		info_ptr->brr = up_baud_to_brr(slot_ptr->baud);
		info_ptr->buffer = slot_ptr->buffer;
		info_ptr->buf_length_ptr = &slot_ptr->buf_length;
		info_ptr->buf_length = slot_ptr->buf_length;

		/* This slot is done now. */
		remaining_slots--;

		/* If there are still remaining slots increment the pointers */
		if (remaining_slots > 0)
		{
			info_ptr++;
			slot_ptr++;
		}
	}

	/* Insert last action (case 2) here with CCR3 = 50.000 in order to make
	 * sure that this action will not be triggered by Capture/Compare inter-
	 * rupt but by update event. Case 2 switches alternate function from
	 * USART6 RX to TIM3 input capture and therefore prepares the reception
	 * of the expected next SYNC. g_ue_ticks is used in tim3_handler's case
	 * 4 to schedule the update event 3us before SYNC.
	 */
	action_ptr->op_mode = 2;
	action_ptr->ccr3 = 50000;
	nom_ticks = mB_TIM3_PERIOD_NOM;
	real_ticks = nom_ticks + (nom_ticks / mB_DEV_DENOMINATOR) +
			(nom_ticks / dyn_denom);
	g_ue_ticks = (uint16_t) real_ticks;
}

/****************************************************************************
 * Name: tim3_handler
 *
 * Description:
 *   This is the handler for the high priority TIM3 interrupt.
 *
 ****************************************************************************/

static void tim3_handler(void)
{
#if mB_DEBUG_GPIO
	/* Set Debug-GPIO */
	setbit32(STM32_GPIOD_BSRR, GPIO_DEBUG_ISR_ON);
#endif

	/* State machine */
	NEXT_ACTION: switch(g_op_mode)
	{

	/* Actively search for SYNC pulse with high resolution */
	case 0:

		/* Get the CCR1 register value */
		g_tim3_ccr1 = getreg16(STM32_TIM3_CCR1);

		/* Acknowledge the update event interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_UIF);

		/* Did a SYNC pulse occur (pulse > 4us && < 6us)?
		 * Then we try to catch SYNC without trigger next time, but by timing.
		 */
		if (g_tim3_ccr1 > mB_TIM3_4US_TICKS && g_tim3_ccr1 < mB_TIM3_6US_TICKS)
		{
			/* Disable reset when trigger occurs */
			clearbit16(STM32_TIM3_SMCR, GTIM_SMCR_SMS_MASK);

			/* Disable TRC for IC2 and enable TI2 instead */
			clearbit16(STM32_TIM3_CCMR1, GTIM_CCMR1_CC2S_MASK);								// Reset Ch2 to output first (this does not affect pin config, so everything is fine)
			setbit16(STM32_TIM3_CCMR1, (GTIM_CCMR_CCS_CCIN1 << GTIM_CCMR1_CC2S_SHIFT));
			setbit16(STM32_TIM3_CCER, GTIM_CCER_CC2E);										// Enable Ch2 capture

			/* Set new prescaler to cover at least 10ms with 16bit.
			 * This setting is not activated immediately, but after the next update event.
			 */
			putreg16(mB_TIM3_PSC_ACT, STM32_TIM3_PSC);

			/* Set new ARR value to trigger next interrupt 15us after SYNC */
			putreg16(mB_TIM3_PERIOD_SYNC4, STM32_TIM3_ARR);

			/* Next case: Compute corrected ARR value to reset 2 us before SYNC. */
			g_op_mode = 1;
		}

		break;

	/* Compute corrected ARR value to reset 2 us before SYNC.
	 * Prepare high resolution and input capture settings for passive SYNC search.
	 */
	case 1:

		/* Acknowledge the update event interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_UIF);

		/* Set new ARR value to reset approx. 3us before SYNC */
		putreg16(g_ue_ticks, STM32_TIM3_ARR);

		/* Reset prescaler for highest resolution.
		 * This setting is not activated immediately, but after the next update event.
		 */
		putreg16(mB_TIM3_PSC_SYNC, STM32_TIM3_PSC);

		/* Next case: Set new ARR and switch to case 3 */
		g_op_mode = 2;

		break;

	/* Set new ARR, configure PC7 to TIM3 CH2IN and switch to case 3 */
	case 2:

		/* Disable Compare interrupts */
		clearbit16(STM32_TIM3_DIER, GTIM_DIER_CC3IE);

		/* Acknowledge the update event interrupt.
		* This should be done early during the ISR, otherwise ISR is possibly retriggered!
		*/
		clearbit16(STM32_TIM3_SR, GTIM_SR_UIF);

		/* Also acknowledge the compare interrupt to be on the safe side.
		 * May there was only an early interrupt during normal operation, which would now trigger
		 * much earlier in high resolution prescaler mode. This might be even before compare interrupts
		 * were disabled because of the interrupt's latency.
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Disable USART6 RE.
		 * Otherwise switching the AF may be interpreted as incoming byte. */
		clearbit32(STM32_USART6_CR1, USART_CR1_RE);

		/* Disable DMA2 Stream 1 */
		clearbit32(STM32_DMA2_S1CR, DMA_SCR_EN);

		/* Configure PC7 to Alternate function 2 -> TIM3 CH2IN */
		clearbit32(STM32_GPIOC_MODER, GPIO_MODER7_MASK);
		clearbit32(STM32_GPIOC_AFRL, GPIO_AFRL7_MASK);
		setbit32(STM32_GPIOC_AFRL, 2 << GPIO_AFRL7_SHIFT);
		setbit32(STM32_GPIOC_MODER, (2 << GPIO_MODER7_SHIFT));

		/* Set new ARR for passive SYNC without clock correction */
		putreg16(mB_TIM3_PERIOD_SYNC2, STM32_TIM3_ARR);

		/* Are there new tables available from higher logic? If yes switch tables
		 * and reset the flag.
		 */
		if(g_messBUSpriv.new_table_avail)
		{
			g_messBUSpriv.new_table_avail = 0;
			if (g_messBUSpriv.active_table)
			{
				g_messBUSpriv.active_table = 0;
			}
			else
				g_messBUSpriv.active_table = 1;
		}

		/* Choose tables for next timeslice according to updated g_isr_active_tables flag */
		if (g_messBUSpriv.active_table)
		{
			isr_actiontable_ptr = isr_actiontable1;
			isr_infotable_ptr = isr_infotable1;
		}
		else
		{
			isr_actiontable_ptr = isr_actiontable0;
			isr_infotable_ptr = isr_infotable0;
		}

		/* Next case: Passively measure SYNC */
		g_op_mode = 3;

		break;

	/* Passively measure SYNC (which normally should have occurred just before).
	 * Also prepare offset correcting update event, as CCR2 may not represent 2us exactly.
	 * Configure PC7 to USART6 RX if we are still synchronized.
	 */
	case 3:

		/* Acknowledge the update event interrupt.
		* This should be done early during the ISR, otherwise ISR is possibly retriggered!
		*/
		clearbit16(STM32_TIM3_SR, GTIM_SR_UIF);

		/* Get the CCR1 and CCR2 register values */
		g_tim3_ccr1 = getreg16(STM32_TIM3_CCR1);
		g_tim3_ccr2 = getreg16(STM32_TIM3_CCR2);

		/* CCR2 register value is the phase */
		int16_t phase_ticks = ((mB_TIM3_3US_TICKS) - g_tim3_ccr2);
		phase_ticks /= (int16_t)(STM32_APB1_TIM3_CLKIN / mB_TIM3_FREQ_ACT);
		g_messBUSpriv.phase_dev = (int8_t) phase_ticks;

		/* Calculate the ticks during SYNC */
		uint16_t sync_ticks = g_tim3_ccr1 - g_tim3_ccr2;

		/* Check if SYNC was in acceptable range and hence successful */
		if (sync_ticks > mB_TIM3_4US_TICKS && sync_ticks < mB_TIM3_6US_TICKS)
		{
			g_sync_err = 0;
		}
		else
		{
			g_sync_err++;
		}

		/* Check if phase of SYNC was in acceptable range and hence successful.
		 * Also set new ARR for passive SYNC with clock offset correction.
		 */
		if (g_tim3_ccr2 > mB_LOWER_PHASE_THR && g_tim3_ccr2 < mB_UPPER_PHASE_THR)
		{
			uint16_t new_arr = mB_TIM3_PERIOD_SYNC3 + g_tim3_ccr2;
			putreg16(new_arr, STM32_TIM3_ARR);
			g_phase_err = 0;
		}
		else
		{
			putreg16(mB_TIM3_PERIOD_SYNC4, STM32_TIM3_ARR);
			g_phase_err++;
		}

		/* Check error counters to determine how to proceed in the next ISR */
		if (g_sync_err <= CONFIG_MESSBUS_MAX_SYNC_ERR && g_phase_err <= CONFIG_MESSBUS_MAX_PHASE_ERR)
		{
			/* Set new prescaler to cover at least 10ms with 16bit.
			* This setting is not activated immediately, but after the next update event.
			*/
			putreg16(mB_TIM3_PSC_ACT, STM32_TIM3_PSC);

			/* Configure PC7 to Alternate function 8 -> USART6 RX */
			clearbit32(STM32_GPIOC_MODER, GPIO_MODER7_MASK);
			clearbit32(STM32_GPIOC_AFRL, GPIO_AFRL7_MASK);
			setbit32(STM32_GPIOC_AFRL, 8 << GPIO_AFRL7_SHIFT);
			setbit32(STM32_GPIOC_MODER, (2 << GPIO_MODER7_SHIFT));

			/* Set baudrate */
			putreg32((uint32_t) isr_infotable_ptr->brr, STM32_USART6_BRR);

			/* Prepare and enable the DMA stream */
			putreg32((uint32_t) isr_infotable_ptr->buffer, mB_USART6RX_DMA_SxMOAR);		// Set the memory address
			putreg32((uint32_t) 0xffff, mB_USART6RX_DMA_SxNDTR);						// Set the NDTR to max 0xffff
			setbit32(mB_USART6RX_DMA_xIFCR, mB_USART6RX_DMA_INT_SxMASK);				// Clear pending flags for safety reasons
			setbit32(mB_USART6RX_DMA_SxCR, DMA_SCR_EN);									// Enable the stream

			/* Enable USART6 RE again */
			setbit32(STM32_USART6_CR1, USART_CR1_RE);

			/* Signal that there was another successful SYNC */
			g_messBUSpriv.sync_count++;

			/* Finally increment relevant table pointers */
			isr_actiontable_ptr++;

			/* Next case: Passively measure SYNC */
			g_op_mode = 4;
		}
		else
		{
			/* Next case: Actively measure SYNC */
			g_op_mode = 9;
		}

		break;

	/* Compute corrected ARR value to reset approx. 3 us before SYNC.
	 * Prepare high resolution and input capture settings for passive SYNC search.
	 */
	case 4:

		/* Acknowledge the update event interrupt.
		* This should be done early during the ISR, otherwise ISR is possibly retriggered!
		*/
		clearbit16(STM32_TIM3_SR, GTIM_SR_UIF);

		/* Set new ARR value to reset approx. 3us before SYNC */
		putreg16(g_ue_ticks, STM32_TIM3_ARR);

		/* Reset prescaler for highest resolution.
		 * This setting is not activated immediately, but after the next update event.
		 */
		putreg16(mB_TIM3_PSC_SYNC, STM32_TIM3_PSC);

		/* Set new CCR3 value for next capture interrupt. */
		putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);

		/* Already acknowledge the compare interrupt for safety reasons. */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Enable Compare interrupts */
		setbit16(STM32_TIM3_DIER, GTIM_DIER_CC3IE);

		/* Next case: First action */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Finally increment relevant table pointers */
		isr_actiontable_ptr++;

		break;

	/* Reconfigure Timer 3 to actively search for SYNC again */
	case 9:

		/* Acknowledge the update event interrupt.
		* This should be done early during the ISR, otherwise ISR is possibly retriggered!
		*/
		clearbit16(STM32_TIM3_SR, GTIM_SR_UIF);

		/* Set new ARR value to 7us */
		putreg16(mB_TIM3_PERIOD_SYNC1, STM32_TIM3_ARR);

		/* Disable TI2 for IC2 and enable TRC instead */
		clearbit16(STM32_TIM3_CCER, GTIM_CCER_CC2E);									// Disable Ch2 capture
		clearbit16(STM32_TIM3_CCMR1, GTIM_CCMR1_CC2S_MASK);								// Reset Ch2 to output first (this does not affect pin config, so everything is fine)
		setbit16(STM32_TIM3_CCMR1, (GTIM_CCMR_CCS_CCINTRC << GTIM_CCMR1_CC2S_SHIFT));

		/* Enable reset when trigger occurs */
		setbit16(STM32_TIM3_SMCR, GTIM_SMCR_RESET);

		/* Next case: Actively measure SYNC again */
		g_op_mode = 0;

		break;

	/* Prepare Rx operation */
	case mB_CH1_RX_START:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Rx start specific operations now...
		 * Set baudrate first:
		 */
		putreg32((uint32_t) isr_infotable_ptr->brr, STM32_USART6_BRR);

		/* Prepare and enable the DMA stream */
		putreg32((uint32_t) isr_infotable_ptr->buffer, mB_USART6RX_DMA_SxMOAR);		// Set the memory address
		putreg32((uint32_t) 0xffff, mB_USART6RX_DMA_SxNDTR);						// Set the NDTR to max 0xffff
		setbit32(mB_USART6RX_DMA_xIFCR, mB_USART6RX_DMA_INT_SxMASK);				// Clear pending flags for safety reasons
		setbit32(mB_USART6RX_DMA_SxCR, DMA_SCR_EN);									// Enable the stream

		/* Enable the Receive Enable bit at the USART */
		setbit32(STM32_USART6_CR1, USART_CR1_RE);

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next ISR or wait for it if it's already too late. Make sure that there are
		 * at least two timer counts remaining until the scheduled next interrupt. One timer
		 * count would not be enough as it could happen that the transition occurs right after
		 * the query of the timer counter register so that we would miss the scheduled interrupt.
		 * If there are less then two timer counts remaining busy wait for the next scheduled
		 * interrupt time and "goto" the switch case routine again. This routine also works when
		 * the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + 1))
		{
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);
			isr_actiontable_ptr++;
		}
		else
		{
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Stop Rx operation */
	case mB_CH1_RX_END:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Rx end specific operations now...
		 * Disable the Receive Enable bit at the USART first. Incomplete transfers will be aborted,
		 * so after enabling the receiver again it will start searching for a start bit again.
		 */
		clearbit32(STM32_USART6_CR1, USART_CR1_RE);

		/* Disable the DMA stream */
		clearbit32(mB_USART6RX_DMA_SxCR, DMA_SCR_EN);

		/* Write the number of received bytes directly into the slotlist (as feedback) */
		*isr_infotable_ptr->buf_length_ptr = 0xffff - ((uint16_t) getreg32(mB_USART6RX_DMA_SxNDTR));

		/* Increment infotable pointer as we have completed this RX slot now. */
		isr_infotable_ptr++;

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next ISR or wait for it if it's already too late. Make sure that there are
		 * at least two timer counts remaining until the scheduled next interrupt. One timer
		 * count would not be enough as it could happen that the transition occurs right after
		 * the query of the timer counter register so that we would miss the scheduled interrupt.
		 * If there are less then two timer counts remaining busy wait for the next scheduled
		 * interrupt time and "goto" the switch case routine again. This routine also works when
		 * the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + 1))
		{
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);
			isr_actiontable_ptr++;
		}
		else
		{
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Tx data enable operation */
	case mB_CH1_TX_DE:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx data enable specific operations now...
		 * Set the Data Enable pin:
		 */
		setbit32(mB_CH1_DE_BSRR, mB_CH1_DE_ON);

		/* Set baudrate */
		putreg32((uint32_t) isr_infotable_ptr->brr, STM32_USART6_BRR);

		/* Already prepare the DMA stream */
		putreg32((uint32_t) isr_infotable_ptr->buffer, mB_USART6TX_DMA_SxMOAR);				// Set the memory address
		putreg32((uint32_t) isr_infotable_ptr->buf_length, mB_USART6TX_DMA_SxNDTR);			// Set the NDTR
		setbit32(mB_USART6TX_DMA_xIFCR, mB_USART6TX_DMA_INT_SxMASK);						// Clear pending flags for safety reasons

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next ISR or wait for it if it's already too late. Make sure that there are
		 * at least two timer counts remaining until the scheduled next interrupt. One timer
		 * count would not be enough as it could happen that the transition occurs right after
		 * the query of the timer counter register so that we would miss the scheduled interrupt.
		 * If there are less then two timer counts remaining busy wait for the next scheduled
		 * interrupt time and "goto" the switch case routine again. This routine also works when
		 * the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + 1))
		{
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);
			isr_actiontable_ptr++;
		}
		else
		{
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Tx start operation */
	case mB_CH1_TX_START:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx start specific operations now...
		 * Enable the previously prepared DMA stream now:
		 */
		setbit32(mB_USART6TX_DMA_SxCR, DMA_SCR_EN);

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next ISR or wait for it if it's already too late. Make sure that there are
		 * at least two timer counts remaining until the scheduled next interrupt. One timer
		 * count would not be enough as it could happen that the transition occurs right after
		 * the query of the timer counter register so that we would miss the scheduled interrupt.
		 * If there are less then two timer counts remaining busy wait for the next scheduled
		 * interrupt time and "goto" the switch case routine again. This routine also works when
		 * the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + 1))
		{
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);
			isr_actiontable_ptr++;
		}
		else
		{
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	/* Tx end operation */
	case mB_CH1_TX_END:

		/* Acknowledge the compare interrupt.
		 * This should be done early during the ISR, otherwise ISR is possibly retriggered!
		 */
		clearbit16(STM32_TIM3_SR, GTIM_SR_CC3IF);

		/* Tx end specific operations now...
		 * Reset the Data Enable pin first:
		 */
		setbit32(mB_CH1_DE_BSRR, mB_CH1_DE_OFF);

		/* Force the DMA stream to disable itself, which should already have happened after
		 * transmission of all bytes according to previous NDTR setting.
		 * Hence, this is only a safety feature to make sure that the next DMA transfer can
		 * start immediately and there is no pending data from the previous transfer. The
		 * disabling procedure may takes some time, as the DMA waits for the current transfer
		 * to complete before actually writing the DMA_SCR_EN bit to zero. But this is ok for
		 * our purposes, as the next transfer will not happen immediately.
		 */
		clearbit32(mB_USART6TX_DMA_SxCR, DMA_SCR_EN);

		/* Increment infotable pointer */
		isr_infotable_ptr++;

		/* Next case: */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Schedule next ISR or wait for it if it's already too late. Make sure that there are
		 * at least two timer counts remaining until the scheduled next interrupt. One timer
		 * count would not be enough as it could happen that the transition occurs right after
		 * the query of the timer counter register so that we would miss the scheduled interrupt.
		 * If there are less then two timer counts remaining busy wait for the next scheduled
		 * interrupt time and "goto" the switch case routine again. This routine also works when
		 * the next interrupt should already have occurred.
		 */
		if (isr_actiontable_ptr->ccr3 > (getreg16(STM32_TIM3_CNT) + 1))
		{
			putreg16(isr_actiontable_ptr->ccr3, STM32_TIM3_CCR3);
			isr_actiontable_ptr++;
		}
		else
		{
			while (isr_actiontable_ptr->ccr3 > getreg16(STM32_TIM3_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}

		break;

	}

#if mB_DEBUG_GPIO
	/* Reset Debug-GPIO */
	setbit32(STM32_GPIOD_BSRR, GPIO_DEBUG_ISR_OFF);
#endif
}

/****************************************************************************
 * Name: up_setup
 *
 * Description:
 *   Setup the hardware but don't start it already.
 *   This method is called by the upper half after a successful call to the
 *   upper half's open method from application logic.
 *
 ****************************************************************************/

static int up_setup(void)
{
	int ret;

#if mB_DEBUG_GPIO
	/* Configure dummy GPIO */
	ret = stm32_configgpio(GPIO_DEBUG_ISR);
	setbit32(STM32_GPIOD_BSRR, GPIO_DEBUG_ISR_ON);
#endif

	/* Attach TIM3 ram vector */
	ret = up_ramvec_attach(STM32_IRQ_TIM3, tim3_handler);
	if (ret < 0)
		{
			// Error handling ???
		}

	/* Set the priority of the TIM3 interrupt vector */
	ret = up_prioritize_irq(STM32_IRQ_TIM3, NVIC_SYSH_HIGH_PRIORITY);
	if (ret < 0)
		{
			// Error handling?
		}

	/* Enable the timer interrupt at the NVIC */
	up_enable_irq(STM32_IRQ_TIM3);

	/* Make sure peripheral clocks are enabled */
	modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM3EN);
	modifyreg32(STM32_RCC_AHB1ENR, 0, RCC_AHB1ENR_DMA2EN);
	modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_USART6EN);

	/* Configure USART related GPIOs as alternate functions.
	 * The Timer3 Ch2In pin is the same as the USART RX pin and hence
	 * must not be configured here!
	 * The USART6 RX configuration provides higher GPIO speed and is therefore
	 * chosen here instead of the Timer3 Ch2In configuration.
	 */
	ret = stm32_configgpio(GPIO_USART6_RX);				// RX and IC on PC7
	ret = stm32_configgpio(GPIO_USART6_TX);				// TX on PC6

	/* Reconfigure PC7 to Alternate function 2 -> TIM3 CH2IN
	 * PC7 still uses GPIO speed as configured for USART6 RX, only the alternate
	 * function is modified here for this pin.
	 */
	clearbit32(STM32_GPIOC_MODER, GPIO_MODER7_MASK);
	clearbit32(STM32_GPIOC_AFRL, GPIO_AFRL7_MASK);
	setbit32(STM32_GPIOC_AFRL, 2 << GPIO_AFRL7_SHIFT);
	setbit32(STM32_GPIOC_MODER, (2 << GPIO_MODER7_SHIFT));

	/* Try to take USART6's Rx DMA stream in the OS. It will be locked with
	 * a semaphore so that no other driver can use the stream. If another
	 * driver already uses the stream stm32_dmachannel waits until the
	 * specified stream is released. This is a safety feature to make sure
	 * the driver has exclusive access to the DMA stream.
	 *
	 * Also initially configure the stream with the method stm32_dmasetup.
	 */
	uint32_t scr;
	DMA_HANDLE usart6rx_dma;
	scr = (DMA_SCR_DIR_P2M | DMA_SCR_MINC | DMA_SCR_PRIVERYHI);
	usart6rx_dma = stm32_dmachannel(DMAMAP_USART6_RX);
#if defined(CONFIG_ARCH_CHIP_STM32F7)
	stm32_dmasetup(usart6rx_dma, STM32_USART6_RDR, 0, 0, scr);
#elif defined(CONFIG_STM32_STM32F4XXX)
	stm32_dmasetup(usart6rx_dma, STM32_USART6_DR, 0, 0, scr);
#endif

	/* Try to take USART6's Tx DMA stream in the OS. It will be locked with
	 * a semaphore so that no other driver can use the stream. If another
	 * driver already uses the stream stm32_dmachannel waits until the
	 * specified stream is released. This is a safety feature to make sure
	 * the driver has exclusive access to the DMA stream.
	 *
	 * Also initially configure the stream with the method stm32_dmasetup.
	 */
	DMA_HANDLE usart6tx_dma;
	scr = (DMA_SCR_DIR_M2P | DMA_SCR_MINC | DMA_SCR_PRIHI);
	usart6tx_dma = stm32_dmachannel(DMAMAP_USART6_TX);
#if defined(CONFIG_ARCH_CHIP_STM32F7)
	stm32_dmasetup(usart6tx_dma, STM32_USART6_TDR, 0, 0, scr);
#elif defined(CONFIG_STM32_STM32F4XXX)
	stm32_dmasetup(usart6tx_dma, STM32_USART6_DR, 0, 0, scr);
#endif

	/* Setup USART6.
	 * Don't configure baudrate already. This will be done via
	 * convert_slotlist and the ISR */
	modifyreg32(STM32_USART6_CR3, 0, USART_CR3_DMAR);										// Enable DMA receiver for USART6
	modifyreg32(STM32_USART6_CR3, 0, USART_CR3_DMAT);										// Enable DMA transmitter for USART6
	modifyreg32(STM32_USART6_CR1, 0, USART_CR1_OVER8);										// Enable oversampling by 8

	return 0;
}

/****************************************************************************
 * Name: up_start
 *
 * Description:
 *   Enable the USART and configure and start timer 3.
 *
 ****************************************************************************/

static int up_start(void)
{
	/* Enable USART 6. The RE bit will be set via Timer 3's ISR. */
	modifyreg32(STM32_USART6_CR1, 0, USART_CR1_UE);											// Enable USART6
	modifyreg32(STM32_USART6_CR1, 0, USART_CR1_TE);											// Enable the transmitter

	/* Configure Timer 3 as active SYNC catcher */
	putreg16(mB_TIM3_PSC_SYNC, STM32_TIM3_PSC);												// Configure prescaler
	putreg16(mB_TIM3_PERIOD_SYNC1, STM32_TIM3_ARR);											// Configure 7us period
	modifyreg16(STM32_TIM3_CCMR1, 0, (GTIM_CCMR_CCS_CCIN2 << GTIM_CCMR1_CC1S_SHIFT));		// Remap Ch1 to Ch2
	modifyreg16(STM32_TIM3_CCMR1, 0, (GTIM_CCMR_CCS_CCINTRC << GTIM_CCMR1_CC2S_SHIFT));		// Ch2 is input for trigger TRC
	modifyreg16(STM32_TIM3_CCER, 0, GTIM_CCER_CC2P);										// Ch2 sensitive to falling edge
	modifyreg16(STM32_TIM3_SMCR, 0, GTIM_SMCR_TI2FP2);										// Ch2 is trigger input
	modifyreg16(STM32_TIM3_SMCR, 0, GTIM_SMCR_RESET);										// Reset mode
	modifyreg16(STM32_TIM3_CR1, 0, GTIM_CR1_URS);											// Update event interrupt only for overflow (not for reset)
	modifyreg16(STM32_TIM3_CCER, 0, GTIM_CCER_CC1E);										// Enable Ch1 capture
	modifyreg16(STM32_TIM3_DIER, 0, GTIM_DIER_UIE);											// Enable update interrupt
	modifyreg16(STM32_TIM3_CR1, 0, GTIM_CR1_CEN);											// Enable the counter

	return 0;
}

/****************************************************************************
 * Name: up_stop
 *
 * Description:
 *   Not provided at this stage of development. Should principally be the
 *   counterpart to up_start.
 *
 ****************************************************************************/

static int up_stop(void)
{
	/* Maybe needed later, so the function is already provided and can be
	 * called from the upper half as it is bound to it via messBUS_ops_s.
	 */

	return 0;
}

/****************************************************************************
 * Name: up_shutdown
 *
 * Description:
 *   Not provided at this stage of development. Should principally be the
 *   counterpart to up_setup.
 *
 ****************************************************************************/

static int up_shutdown(void)
{
	/* Maybe needed later, so the function is already provided and can be
	 * called from the upper half as it is bound to it via messBUS_ops_s.
	 */

	return 0;
}

/****************************************************************************
 * Name: up_update_buffers
 *
 * Description:
 *   Invalidates the F7 chip's data cache to make sure the CPU uses the
 *   newest rx buffers. As only the F7 architecture has a cache, this
 *   function is only provided for F7 chips.
 *
 *   This is a rather bad workaround, because the call consumes about 10us.
 *   A better approach would be to use the MPU to declare the buffers
 *   uncacheable within the application.
 *
 *   Another problem is that the hardware abstraction of the upper half is
 *   partly violated, because it has to know whether a F7 chip is used or not.
 *   This can only be overcome by having a buffer inside the driver itself
 *   parsing the data to the destination buffers via a memcpy and not via DMA.
 *   This approach would on the one hand certainly decrease efficiency and
 *   increase latencies on the other hand.
 *
 ****************************************************************************/


static void up_update_buffers(void)
{
#ifdef CONFIG_ARCH_CHIP_STM32F7
	/* Invalidate the whole cache. This method takes approx. 10 us which
	 * is quite a long time. This could be improved by using single cache
	 * lines or the MPU.
	 */
	arch_invalidate_dcache_all();
#endif
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

void messBUSClient_initialize(void)
{
	/* Perform registration of the driver.
	 * This method should be called from board-specific stm32_bringup.c
	 */
	int ret;

	/* Call the upper half to register the whole driver in the VFS. */
	ret = messBUSClient_register("/dev/messBusClient", &g_messBUSpriv);
	if (ret < 0)
	{
		// Error handling??
	}
}

