/************************************************************************************
 * arch/arm/src/stm32/stm32_messBUSClient.h
 *
 *   Author: Peer Ulbig <p.ulbig@tu-braunschweig.de>
 *
 ************************************************************************************/

#ifndef _ARCH_ARM_SRC_STM32F7_STM32_MESSBUSCLIENT_H_
#define _ARCH_ARM_SRC_STM32F7_STM32_MESSBUSCLIENT_H_

/****************************************************************************
 * Includes
 ****************************************************************************/

#include <sys/types.h>
#include <arch/board/board.h>
#include <arch/chip/chip.h>
#include <nuttx/messBUS/messBUSClient.h>

/****************************************************************************
 * Auslagerungen für board.h
 ****************************************************************************/

#define GPIO_USART6_TX		GPIO_USART6_TX_1
#define GPIO_USART6_RX		GPIO_USART6_RX_1

#define DMAMAP_USART6_RX	DMAMAP_USART6_RX_1
#define DMAMAP_USART6_TX	DMAMAP_USART6_TX_1

/* Data Enable pin (active high) */
#define GPIO_CH1_DE      	(GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
								GPIO_OUTPUT_CLEAR|GPIO_PORTD|GPIO_PIN13)

/****************************************************************************
 * Debug helpers during development of driver (helpful with oscilloscope)
 ****************************************************************************/

#define mB_DEBUG_VISUALIZE_INTERRUPTS						1
#define mB_DEBUG_VISUALIZE_SYNC_TRIGGER_WITH_TIM1_OPM		1

/****************************************************************************
 * Helper #defines
 ****************************************************************************/

#define mB_SEC_2_USEC		1000000ul
#define mB_USEC_2_NSEC		1000

/*
 * messBUS node need to begin offset microseconds before specified rx slot begin with 
 * its preparation to ensure that it is really ready to read data at slot begin.
 */
#define mB_RX_OFFSET_US     -1

/****************************************************************************
 * messBUS
 ****************************************************************************/

/* messBUS SYNC duration is fixed to 5us */
#define mB_SYNC				5

/* Maximum supported slots and channels.
 * Usually you would need two actions per slot except for writes where there is
 * a further Data Enable action required before. */
#define mB_MAX_TX_SLOTS		10
#define mB_MAX_RX_SLOTS		10

/* Normal operation modes of timer 3 interrupt handler */
#define mB_CH1_RX_START		10
#define mB_CH1_RX_END		11
#define mB_CH1_TX_DE		12
#define mB_CH1_TX_START		13
#define mB_CH1_TX_END		14

/****************************************************************************
 * Timer 3 (SYNC Catcher + ISR)
 ****************************************************************************/

/* High resolution settings (SYNC, CK_CNT = TIM3_CLKIN) */
#define mB_TIM3_PSC_SYNC 	(uint16_t) 0															// Set Prescaler to zero to achieve highest resolution
#define mB_TIM3_ISR_SEPA1	(mB_SYNC + 2)															// ISR separation in us (7us) (active search)
#define mB_TIM3_ISR_SEPA2	(mB_SYNC + 5)															// ISR separation in us (9us) (passive search)
#define mB_TIM3_ISR_SEPA3	(mB_SYNC)																// ISR separation in us (5us) (passive search)
#define mB_TIM3_ISR_SEPA4	(mB_SYNC + 3)															// ISR separation in us (8us) (passive search)

#define mB_TIM3_PERIOD_SYNC1 ((((STM32_APB1_TIM3_CLKIN) * mB_TIM3_ISR_SEPA1) / mB_SEC_2_USEC) - 1)	// ARR value during active SYNC (7us)
#define mB_TIM3_PERIOD_SYNC2 ((((STM32_APB1_TIM3_CLKIN) * mB_TIM3_ISR_SEPA2) / mB_SEC_2_USEC) - 1)	// First ARR value during passive SYNC (9us)
#define mB_TIM3_PERIOD_SYNC3 ((((STM32_APB1_TIM3_CLKIN) * mB_TIM3_ISR_SEPA3) / mB_SEC_2_USEC) - 1)	// 2nd ARR value during passive SYNC (6us), enabling low resolution 15us after falling edge of SYNC
#define mB_TIM3_PERIOD_SYNC4 ((((STM32_APB1_TIM3_CLKIN) * mB_TIM3_ISR_SEPA4) / mB_SEC_2_USEC) - 1)	// ARR value during passive SYNC, if SYNC got lost (8us)

#define mB_TIM3_SYNC_THR	((((STM32_APB1_TIM3_CLKIN) * mB_SYNC_THR) / mB_SEC_2_USEC)  - 1)		// Minimum ticks to accept CCR value as possible SYNC (4us)
#define mB_TIM3_SYNC_TICKS	(((STM32_APB1_TIM3_CLKIN) * mB_SYNC) / mB_SEC_2_USEC) 					// Nominal SYNC ticks (no clock deviation)
#define mB_TIM3_1US_TICKS	(((STM32_APB1_TIM3_CLKIN) * 1) / mB_SEC_2_USEC)							// Nominal ticks per us (no clock deviation)
#define mB_TIM3_3US_TICKS	(((STM32_APB1_TIM3_CLKIN) * 3) / mB_SEC_2_USEC)							// Nominal ticks per 3us (no clock deviation)
#define mB_TIM3_4US_TICKS	(((STM32_APB1_TIM3_CLKIN) * 4) / mB_SEC_2_USEC)							// Nominal ticks per 4us (no clock deviation)
#define mB_TIM3_6US_TICKS	(((STM32_APB1_TIM3_CLKIN) * 6) / mB_SEC_2_USEC)							// Nominal ticks per 6us (no clock deviation)

/* Acceptable phase ticks according to CONFIG_MESSBUS_PHASE_TOLERANCE
 * [ns].
 */
#define mB_TOLERATED_TICKS	((((STM32_APB1_TIM3_CLKIN) / mB_SEC_2_USEC) * CONFIG_MESSBUS_PHASE_TOLERANCE) / mB_USEC_2_NSEC)
#define mB_LOWER_PHASE_THR	(mB_TIM3_3US_TICKS - mB_TOLERATED_TICKS)
#define mB_UPPER_PHASE_THR	(mB_TIM3_3US_TICKS + mB_TOLERATED_TICKS)

/* Low resolution settings (CK_CNT = 4 MHz) */
#define mB_TIM3_FREQ_ACT	4000000ul  																					// Desired frequency of Timer 3 when performing action list -> 0.25us resolution
#define mB_TIM3_PSC_ACT 	(uint16_t) ((((STM32_APB1_TIM3_CLKIN) + (mB_TIM3_FREQ_ACT / 2)) / mB_TIM3_FREQ_ACT) - 1)	// Required prescaler setting according to clocking defined in board.h (with rounding)
#define mB_TIM3_PERIOD_NOM	(((10000ul - mB_TIM3_ISR_SEPA1 - mB_TIM3_ISR_SEPA4 - 3) * 4) - 1)							// ARR value to reset 2us before SYNC, 4 ticks/us
#define mB_OFFSET_TICKS		(15*4)																						// CCR3 mode starts 15 us after SYNC and has to be corrected accordingly

/* Static correction for known frequency deviation according to board.h's
 * mB_TICKS_PER_TIMESLICE.
 */
#define mB_DEV_TICKS		(mB_TICKS_PER_TIMESLICE % 40000)
#if (mB_DEV_TICKS == 0)
#define mB_DEV_DENOMINATOR	(50000)	// No ticks will be added then
#elif (mB_DEV_TICKS > 1000)
#define mB_DEV_DENOMINATOR	(40000 / (mB_DEV_TICKS - 40000)) // ticks will be subtracted
#else
#define mB_DEV_DENOMINATOR	(40000 / mB_DEV_TICKS) // ticks will be added
#endif

/****************************************************************************
 * Timer 4 (Slave of Timer 3 for synchronization with the master)
 ****************************************************************************/

/* Let timer 4 run at 4 MHz like timer 2 */
#define mB_TIM4_FREQ_ACT	mB_TIM3_FREQ_ACT
#define mB_TIM4_PSC_ACT 	(uint16_t) ((((STM32_APB1_TIM4_CLKIN) + (mB_TIM4_FREQ_ACT / 2)) / mB_TIM4_FREQ_ACT) - 1)	// Required prescaler setting according to clocking defined in board.h (with rounding)

/****************************************************************************
 * DMA for USART6 Rx
 ****************************************************************************/

#if (DMAMAP_USART6_RX == DMAMAP_USART6_RX_1)
	#define mB_USART6RX_DMA_SxNDTR		STM32_DMA2_S1NDTR
	#define mB_USART6RX_DMA_SxCR		STM32_DMA2_S1CR
	#define mB_USART6RX_DMA_xIFCR		STM32_DMA2_LIFCR
	#define mB_USART6RX_DMA_SxMOAR		STM32_DMA2_S1M0AR
	#define mB_USART6RX_DMA_INT_SxMASK	DMA_INT_STREAM1_MASK
#elif (DMAMAP_USART6_RX == DMAMAP_USART6_RX_2)
	#define mB_USART6RX_DMA_SxNDTR		STM32_DMA2_S2NDTR
	#define mB_USART6RX_DMA_SxCR		STM32_DMA2_S2CR
	#define mB_USART6RX_DMA_xIFCR		STM32_DMA2_LIFCR
	#define mB_USART6RX_DMA_SxMOAR		STM32_DMA2_S2M0AR
	#define mB_USART6RX_DMA_INT_SxMASK	DMA_INT_STREAM2_MASK
#endif

/****************************************************************************
 * DMA for USART6 Tx
 ****************************************************************************/

#if (DMAMAP_USART6_TX == DMAMAP_USART6_TX_1)
	#define mB_USART6TX_DMA_SxNDTR		STM32_DMA2_S6NDTR
	#define mB_USART6TX_DMA_SxCR		STM32_DMA2_S6CR
	#define mB_USART6TX_DMA_xIFCR		STM32_DMA2_HIFCR
	#define mB_USART6TX_DMA_SxMOAR		STM32_DMA2_S6M0AR
	#define mB_USART6TX_DMA_INT_SxMASK	DMA_INT_STREAM6_MASK
#elif (DMAMAP_USART6_TX == DMAMAP_USART6_TX_2)
	#define mB_USART6TX_DMA_SxNDTR		STM32_DMA2_S7NDTR
	#define mB_USART6TX_DMA_SxCR		STM32_DMA2_S7CR
	#define mB_USART6TX_DMA_xIFCR		STM32_DMA2_HIFCR
	#define mB_USART6TX_DMA_SxMOAR		STM32_DMA2_S7M0AR
	#define mB_USART6TX_DMA_INT_SxMASK	DMA_INT_STREAM7_MASK
#endif

/****************************************************************************
 * Ch1 Data Enable pin
 ****************************************************************************/

/* Extract the pin number */
#define mB_CH1_DE_ON			(1 << (GPIO_CH1_DE & GPIO_PIN_MASK))
#define mB_CH1_DE_OFF			(1 << ((GPIO_CH1_DE & GPIO_PIN_MASK)+16))

/* Check for F4 or F7 architecture */
#if defined(CONFIG_ARCH_CHIP_STM32F7)
#define mB_NGPIO_PORTS	STM32F7_NGPIO
#else
#define mB_NGPIO_PORTS	STM32_NGPIO_PORTS
#endif

/* Get the right BSRR address according to port configuration */
#if mB_NGPIO_PORTS > 0
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTA)
#define mB_CH1_DE_BSRR		STM32_GPIOA_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 1
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTB)
#define mB_CH1_DE_BSRR		STM32_GPIOB_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 2
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTC)
#define mB_CH1_DE_BSRR		STM32_GPIOC_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 3
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTD)
#define mB_CH1_DE_BSRR		STM32_GPIOD_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 4
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTE)
#define mB_CH1_DE_BSRR		STM32_GPIOE_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 5
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTF)
#define mB_CH1_DE_BSRR		STM32_GPIOF_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 6
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTG)
#define mB_CH1_DE_BSRR		STM32_GPIOG_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 7
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTH)
#define mB_CH1_DE_BSRR		STM32_GPIOH_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 8
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTI)
#define mB_CH1_DE_BSRR		STM32_GPIOI_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 9
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTJ)
#define mB_CH1_DE_BSRR		STM32_GPIOJ_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 10
#if ((GPIO_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTK)
#define mB_CH1_DE_BSRR		STM32_GPIOK_BSRR
#endif
#endif

/****************************************************************************
 * Debug GPIOs
 ****************************************************************************/

/* This pin has to be belong to Port D */
#define GPIO_DEBUG_ISR     (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
							GPIO_OUTPUT_CLEAR|GPIO_PORTD|GPIO_PIN7)
#define GPIO_DEBUG_ISR_ON	(1 << 7)
#define GPIO_DEBUG_ISR_OFF	(1 << (7+16))

#endif /* _ARCH_ARM_SRC_STM32F7_STM32_MESSBUSCLIENT_H_ */
