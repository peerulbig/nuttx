#
# For a description of the syntax of this configuration file,
# see the file kconfig-language.txt in the NuttX tools repository.
#

if ARCH_BOARD_MB_IMU

choice
	prompt "Select Console wiring."
	default MB_IMU_CONSOLE_RS232
	---help---
		Select where you will connect the console.

		RS232 COM Port:

				STM32F7
			CONNECTOR FUNCTION  GPIO
			-- ------ --------- ----
			J10-1     USART6_RX PC7
			J10-2     USART6_TX PC6
			-- ------ --------- ---

					OR

		UART Connector:

				STM32F7
			CONNECTOR FUNCTION  GPIO
			--------- --------- -----
			J11-2     USART7_RX PB3
			J11-3     USART7_TX PA15
			--------- --------- -----

config MB_IMU_CONSOLE_RS232
	bool "RS232 Connector"
	select STM32F7_USART6
	select USART6_SERIALDRIVER
	select USART6_SERIAL_CONSOLE

config MB_IMU_CONSOLE_UART
	bool "UART Connector"
	select STM32F7_UART7
	select UART7_SERIALDRIVER
	select UART7_SERIAL_CONSOLE

config MB_IMU_CONSOLE_NONE
	bool "No Console"

endchoice # "Select Console wiring"

config MB_IMU_SPI_TEST
	bool "Enable SPI test"
	default n
	---help---
		Enable Spi test - initalize and configure SPI to send
		MB_IMU_SPI_TEST_MESSAGE text. The text is sent on the
		selected SPI Buses with the configured parameters.
		Note the CS lines will not be asserted.

if MB_IMU_SPI_TEST

config MB_IMU_SPI_TEST_MESSAGE
	string "Text to Send on SPI Bus(es)"
	default "Hello World"
	depends on MB_IMU_SPI_TEST
	---help---
		Text to sent on SPI bus(es)

config MB_IMU_SPI4_TEST
	bool "Test SPI bus 4"
	default n
	depends on MB_IMU_SPI_TEST
	---help---
		Enable Spi test - on SPI BUS 4

if MB_IMU_SPI4_TEST

config MB_IMU_SPI4_TEST_FREQ
	int "SPI 4 Clock Freq in Hz"
	default 1000000
	depends on MB_IMU_SPI4_TEST
	---help---
		Sets SPI 4 Clock Freq

config MB_IMU_SPI4_TEST_BITS
	int "SPI 4 number of bits"
	default 8
	depends on MB_IMU_SPI4_TEST
	---help---
		Sets SPI 4 bit length

choice
	prompt "SPI BUS 4 Clock Mode"
	default MB_IMU_SPI4_TEST_MODE3
	---help---
		Sets SPI 4 clock mode

config MB_IMU_SPI4_TEST_MODE0
	bool "CPOL=0 CHPHA=0"

config MB_IMU_SPI4_TEST_MODE1
	bool "CPOL=0 CHPHA=1"

config MB_IMU_SPI4_TEST_MODE2
	bool "CPOL=1 CHPHA=0"

config MB_IMU_SPI4_TEST_MODE3
	bool "CPOL=1 CHPHA=1"

endchoice # "SPI BUS 4 Clock Mode"

endif # MB_IMU_SPI4_TEST

endif # MB_IMU_SPI_TEST

config MB_IMU_ADIS16XXX
	bool "Enable ADIS16XXX driver for the IMU on mB_IMU"
	default n
	depends on SPI
	depends on SENSORS_ADIS16XXX
	default n
	---help---
		Select to create a ADIS16XXX driver instance for the onboard imu on mB_IMU.
		Provides /dev/imu0 device file.

endif # ARCH_BOARD_MB_IMU
